rm -rf spa
cd ../../src/frontend
npm install
npm run build
cd dist
cp ../../../continous-integration/frontend/nginx.conf ./nginx.conf
cp ../../../continous-integration/frontend/fullchain.pem ./fullchain.pem
cp ../../../continous-integration/frontend/privkey.pem ./privkey.pem
docker build . -f ../../../continous-integration/frontend/Dockerfile -t teletutoring_frontend:0.2
cd ../../../continous-integration/frontend/
