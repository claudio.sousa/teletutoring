import bcrypt from 'bcrypt';
import shortid from 'shortid';
import { mongoDB } from '../db/MongoDB';
import { IUser, userService } from '../user';

export interface IAuthentication {
    username: string;
    password: string;
}

export interface IUserSession {
    userId: string;
    authenticated: boolean;
}

class AuthenticationService {

    public async createCredential(userId: string, username: string, password: string) {
        const hash = await bcrypt.hash(password, 10);
        const credentials = await mongoDB.credentialsCollection;
        credentials.insert({ id: shortid(), userId, username, password: hash });
    }

    public async login(username: string, password: string, userSession: IUserSession): Promise<IUser | null> {
        const credentials = await mongoDB.credentialsCollection;
        const user = await credentials
            .findOne({ username });

        if (!user) {
            return null;
        }
        const correct = await bcrypt.compare(password, user.password);
        if (!correct) {
            return null;
        }

        userSession.authenticated = true;
        userSession.userId = user.userId;
        return userService.findOne(user.userId);
    }
}

export const authenticateService: AuthenticationService = new AuthenticationService();
