import { NextFunction, Request, Response, Router } from 'express';
import { IUserSession } from './AuthenticationService';

const router: Router = Router();

router.use((req: Request, res: Response, next: NextFunction) => {

    if (!req.session || !req.session.userSession) {
        res.status(403);
        return next('Unauthorized access!');
    }
    const userSession = req.session.userSession as IUserSession;
    if (!userSession.authenticated) {
        res.status(403);
        return next('Unauthorized access!');
    }
    next();
});

export const loginRestrictedController: Router = router;
