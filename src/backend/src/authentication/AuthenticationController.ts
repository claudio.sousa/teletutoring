import { Request, Response, Router } from 'express';
import { authenticateService, IAuthentication, IUserSession } from './AuthenticationService';

const router: Router = Router();

router.post('/logout', async (req: Request, res: Response) => {
    if (!req.session) {
        return res.status(500).send('No session found');
    }

    req.session.destroy(() => {
        res
            .clearCookie('connect.sid')
            .sendStatus(200);
    });
});

router.post('/login', async (req: Request, res: Response) => {
    const authentication = req.body as IAuthentication;

    if (!req.session) {
        return res.status(500).send('No session found');
    }

    req.session.userSession = { authenticated: false } as IUserSession;

    const sucessful = await authenticateService.login(authentication.username,
        authentication.password, req.session.userSession as IUserSession);

    res.send({
        data: sucessful
    });
});

export const authenticationController: Router = router;