import shortid from 'shortid';
import { mongoDB } from '../db/MongoDB';
import { IUser, userService } from '../user';

export enum SessionStatusType {
    requested = 0,
    confirmed = 1,
    ongoing = 2,
    done = 3,
    canceled = 4,
    rejected = 5
}

export interface ITutorClass {
    subject: string;
    levels: string[];
    price: number;
}

export interface ISessionDB {
    id: string;
    date: number;
    tutoredClass: ITutorClass;
    duration: number;
    tutorId: string;
    learnerId: string;
    status: SessionStatusType;
}

interface ISession extends ISessionDB {
    tutor: IUser | null;
    learner: IUser | null;
}

class SessionService {

    public async insertSession(session: ISessionDB) {
        const id = shortid();
        session.id = id;
        const sessions = await mongoDB.sessionsCollection;
        sessions.insert({ id, ...session });
        return id;
    }

    public async getSessions(userId: string): Promise<ISession[]> {
        const sessions = await mongoDB.sessionsCollection;
        const foundSessions = await sessions.find({ $or: [{ tutorId: userId }, { learnerId: userId }] }).toArray();
        return Promise.all(foundSessions.map((session: ISessionDB) => this.fillSessionInfo(session)));
    }

    public async getChildrenSessions(parentId: string): Promise<ISession[]> {
        const sessions = await mongoDB.sessionsCollection;
        const children = await userService.findChildren(parentId);
        const childSessions = await sessions.find({ learnerId: { $in: children.map((child) => child.id) } }).toArray();

        return Promise.all(childSessions.map((session: ISessionDB) => this.fillSessionInfo(session)));
    }

    public async updateSession(sessionId: string, newSession: ISessionDB) {
        const sessions = await mongoDB.sessionsCollection;
        return sessions.findOneAndReplace({ id: sessionId }, newSession);
    }

    private async fillSessionInfo(session: ISessionDB): Promise<ISession> {
        return {
            ...session,
            tutor: await userService.findOne(session.tutorId),
            learner: await userService.findOne(session.learnerId)
        };
    }
}

export const sessionService: SessionService = new SessionService();
