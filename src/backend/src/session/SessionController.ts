import { Request, Response, Router } from 'express';
import { ISessionDB, sessionService } from './SessionService';

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
    if (!req.session) {
        return res.status(500).send('No session found');
    }

    res.send({
        data: await sessionService.getSessions(req.session.userSession.userId as string)
    });
});

router.get('/children', async (req: Request, res: Response) => {
    if (!req.session) {
        return res.status(500).send('No session found');
    }

    res.send({
        data: await sessionService.getChildrenSessions(req.session.userSession.userId as string)
    });
});

router.post('/', async (req: Request, res: Response) => {
    res.send({
        data: await sessionService.insertSession(req.body as ISessionDB)
    });
});

router.put('/:sessionId', async (req: Request, res: Response) => {
    res.send({
        data: await sessionService.updateSession(req.params.sessionId as string, req.body as ISessionDB)
    });
});

export const sessionController: Router = router;
