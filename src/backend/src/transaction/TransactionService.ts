import { mongoDB } from '../db/MongoDB';
import { userService } from '../user';

export interface ITransaction {
    userId: string;
    amount: number;
    timestamp: number;
}

class TransactionService {

    public async getTransactions(userId: string): Promise<ITransaction[]> {
        const transactions = await mongoDB.transactionsCollection;
        return transactions.find({ userId }).toArray();
    }

    public async addTransaction(userId: string, amount: number): Promise<number | null> {

        const transactions = await mongoDB.transactionsCollection;
        transactions.insert({ userId, amount, timestamp: Date.now() });

        return userService.creditAccount(userId, amount);
    }
}

export const transactionService: TransactionService = new TransactionService();