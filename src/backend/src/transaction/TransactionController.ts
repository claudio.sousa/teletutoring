import { Request, Response, Router } from 'express';
import { transactionService } from './TransactionService';

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
    if (!req.session) {
        return res.status(500).send('No session found');
    }

    res.send({
        data: await transactionService.getTransactions(req.session.userSession.userId as string)
    });
});

router.post('/credit', async (req: Request, res: Response) => {
    if (!req.session) {
        return res.status(500).send('No session found');
    }

    const { amount } = req.body as { amount: number };
    res.send({
        data: await transactionService.addTransaction(req.session.userSession.userId as string, amount)
    });
});

export const transactionController: Router = router;
