import shortid from 'shortid';
import { IUser, userService, UserType } from '../user';

export interface IAvailability {
    id: string;
    daysOfWeek: number[];
    startTime: number;
    endTime: number;
    startRecur: number;
    endRecur: number;
}

export interface ITutor extends IUser {
    verified: boolean;
    availableNow: boolean;
    rating: number;
    classes: Array<{
        subject: string,
        levels: string[],
        price: number
    }>;
    availabilities: IAvailability[];
}

interface IFilterAvailability {
    daysOfWeek?: number[]; // eg. [0, 2, 6],
    hour_range?: number[]; // [start, end]. eg: [0, 24]
}

interface ITutorCustomFilter {
    price_range?: number[];
    subject?: string;
    level?: string;
    availability?: IFilterAvailability;
}

class TutorService {
    public getTutors() {
        return this.findTutors();
    }

    public findTutors(tutor?: any) {
        tutor = tutor || {};
        const customFilter = tutor.filter;
        delete tutor.filter;
        return userService
            .findUsers(
                { ...tutor, type: UserType.tutor },
                (user: IUser) => this.filterTutor(customFilter, user as ITutor));
    }

    public updateTutor(tutorId: string, tutor: ITutor) {
        return userService.updateUser(tutorId, tutor);
    }

    public deleteTutor(tutorId: string) {
        userService.deleteUser(tutorId);
    }

    public async getAvailabilities(tutorId: string): Promise<IAvailability[]> {
        const tutor = await this.getTutor(tutorId);
        return tutor.availabilities;
    }

    public async addAvailability(tutorId: string, availability: IAvailability) {
        const tutor = await this.getTutor(tutorId);
        const id = shortid();
        tutor.availabilities.push({ ...availability, id });
        await userService.updateUser(tutor.id, tutor);
        return id;
    }

    public async deleteAvailability(tutorId: string, availabilityId: string) {
        const tutor = await this.getTutor(tutorId);
        tutor.availabilities = tutor.availabilities.filter((availability) => availability.id === availabilityId);
        return userService.updateUser(tutor.id, tutor);
    }

    public async updateAvailability(tutorId: string, availabilityId: string, newAvailability: IAvailability) {
        const tutor = await this.getTutor(tutorId);
        tutor.availabilities = tutor.availabilities.map((availability) =>
            availability.id === availabilityId ? newAvailability : availability
        );
        return userService.updateUser(tutor.id, tutor);
    }

    public async getTutor(tutorId: string): Promise<ITutor> {
        const tutor = await userService.findOne(tutorId) as ITutor;
        if (!tutor) {
            throw new Error(`Tutor ${tutorId} not found`);
        }
        return tutor;
    }

    private filterAvailability(tutor: ITutor, availabilityFilter?: IFilterAvailability): boolean {
        if (!availabilityFilter) {
            return true;
        }
        const daysFilter = availabilityFilter.daysOfWeek && new Set(availabilityFilter.daysOfWeek);
        const timeFilter = availabilityFilter.hour_range;
        return tutor.availabilities.some((availability) => {
            if (daysFilter && !availability.daysOfWeek.some((day) => daysFilter.has(day))) {
                return false;
            }
            if (timeFilter) {
                const startTime = new Date(availability.startTime).getHours();
                const endTime = new Date(availability.endTime).getHours();
                if (startTime > timeFilter[1] || endTime < timeFilter[0]) {
                    return false;
                }
            }
            return true;
        });
    }

    private filterSubjet(tutor: ITutor, subject?: string, level?: string, priceRange?: number[]): boolean {
        return tutor.classes.some((c) => {
            if (subject && c.subject !== subject) {
                return false;
            }
            if (level && !c.levels.includes(level)) {
                return false;
            }
            if (priceRange && (c.price < priceRange[0] || c.price > priceRange[1])) {
                return false;
            }
            return true;
        });
    }

    private filterTutor(customFilter: ITutorCustomFilter, tutor: ITutor): boolean {
        if (!customFilter) {
            return true;
        }
        if (!this.filterSubjet(tutor, customFilter.subject, customFilter.level, customFilter.price_range)) {
            return false;
        }
        if (!this.filterAvailability(tutor, customFilter.availability)) {
            return false;
        }
        return true;
    }

}

export const tutorService: TutorService = new TutorService();