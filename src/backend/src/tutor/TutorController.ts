import { Request, Response, Router } from 'express';
import { IAvailability, ITutor, tutorService } from './TutorService';

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.getTutors()
    });
});

router.get('/:tutorId', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.getTutor(req.params.tutorId as string)
    });
});

router.delete('/:tutorId', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.deleteTutor(req.params.tutorId as string)
    });
});

router.put('/:tutorId', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.updateTutor(req.params.tutorId as string, req.body as ITutor)
    });
});

router.post('/search', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.findTutors(req.body)
    });
});

router.get('/:tutorId/availability', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.getAvailabilities(req.params.tutorId as string)
    });
});

router.post('/:tutorId/availability', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.addAvailability(req.params.tutorId as string, req.body as IAvailability)
    });
});

router.put('/:tutorId/availability/:availabilityId', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.updateAvailability(
            req.params.tutorId as string,
            req.params.availabilityId as string,
            req.body as IAvailability)
    });
});

router.delete('/:tutorId/availability/:availabilityId', async (req: Request, res: Response) => {
    res.send({
        data: await tutorService.deleteAvailability(req.params.tutorId as string, req.params.availabilityId as string)
    });
});

export const tutorController: Router = router;
