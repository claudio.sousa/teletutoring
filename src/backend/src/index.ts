import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import session from 'express-session';

import { authenticationController, loginRestrictedController } from './authentication/index';
import { lookupController } from './lookup/index';
import { reviewController } from './review/index';
import { sessionController } from './session/index';
import { transactionController } from './transaction';
import { tutorController } from './tutor/index';
import { userController } from './user/index';

const app = express();
const port: number = Number(process.env.PORT) || 3001;

app.use(cors());
app.use(bodyParser.json());

app.use(session({ secret: 'mysecret', cookie: { maxAge: 60 * 60 * 1000 } }));

app.use('/authentication', authenticationController as any);

app.use(loginRestrictedController);

app.use('/sessions', sessionController as any);
app.use('/tutors', tutorController as any);
app.use('/users', userController as any);
app.use('/lookups', lookupController as any);
app.use('/reviews', reviewController as any);
app.use('/transactions', transactionController as any);

// start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
