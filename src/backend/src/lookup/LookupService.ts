const LOOKUPS = {
    subjects: [
        'MATH',
        'FRANCAIS',
        'ANGLAIS',
        'ALLEMAND',
        'SCIENCES_NATURE',
        'HISTOIRE',
        'GEOGRAPHIE',
        'CITOYENNETE',
        'ARTS_MANUELLES',
        'ARTS_VISUELS',
        'MUSIQUE',
        'EDUCATION_PHYSIQUE'
    ],
    levels: [
        '1P',
        '2P',
        '3P',
        '4P',
        '5P',
        '6P',
        '7P',
        '8P',
        '9P',
        '10P',
        '11P'
    ],
    languages: [
        'FR',
        'EN',
        'DE',
        'IT'
    ]
};
class LookupService {

    public subjects() {
        return this.lookupValues('subjects');
    }

    public levels() {
        return this.lookupValues('levels');
    }

    public languages() {
        return this.lookupValues('languages');
    }

    private lookupValues(lookupType: string): string[] {
        return (LOOKUPS as any)[lookupType];
    }
}

export const lookupService: LookupService = new LookupService();