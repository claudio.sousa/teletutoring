import { Request, Response, Router } from 'express';
import { lookupService } from './LookupService';

const router: Router = Router();

router.get('/subjects', async (req: Request, res: Response) => {
    res.send({
        data: await lookupService.subjects()
    });
});

router.get('/levels', async (req: Request, res: Response) => {
    res.send({
        data: await lookupService.levels()
    });
});

router.get('/languages', async (req: Request, res: Response) => {
    res.send({
        data: await lookupService.languages()
    });
});

export const lookupController: Router = router;
