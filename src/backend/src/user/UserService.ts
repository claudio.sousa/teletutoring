import shortid from 'shortid';
import { mongoDB } from '../db/MongoDB';

export enum UserType {
    tutor = 0,
    student = 1,
    parent = 2
}
export interface IUser {
    id: string;
    parentId: string | null;
    firstName: string;
    lastName: string;
    email: string;
    credit: number;
    rating: number;
    ratings: number;
    lastConnection: number;
    languages: string[];
    description: string;
    type: UserType;
}
class UserService {

    public async insertUser(user: IUser) {
        const users = await mongoDB.usersCollection;
        users.insert({ id: shortid(), ...user });
    }

    public async deleteUser(userId: string) {
        const users = await mongoDB.usersCollection;
        users.deleteOne({ id: userId });
    }

    public async getUsers(): Promise<IUser[]> {
        const users = await mongoDB.usersCollection;
        return users.find().toArray();
    }

    public async updateUser(userId: string, newUser: IUser): Promise<IUser> {
        const users = await mongoDB.usersCollection;
        const updated = await users.findOneAndReplace({ id: userId }, newUser);
        return updated.value;
    }

    public async findOne(id: string): Promise<IUser | null> {
        const users = await mongoDB.usersCollection;
        return await users.findOne({ id });
    }

    public async getPhoto(userId: string): Promise<string | null> {
        const photos = await mongoDB.photosCollection;
        const photo = await photos.findOne({ userId });
        return photo && photo.image;
    }

    public async findUsers(user: IUser, filterFn?: (user: IUser) => boolean): Promise<IUser[]> {
        const users = await mongoDB.usersCollection;
        const foundUsers = await users.find(user).toArray();
        return filterFn ? foundUsers.filter(filterFn) : foundUsers;
    }

    public async findChildren(parentId: string): Promise<IUser[]> {
        const users = await mongoDB.usersCollection;
        return await users.find({ parentId }).toArray();
    }

    public async creditAccount(userId: string, credit: number): Promise<number | null> {
        const user = await this.findOne(userId);
        if (!user) {
            return null;
        }
        if (!user.credit) {
            user.credit = 0;
        }
        user.credit += credit;
        await this.updateUser(userId, user);
        return user.credit;
    }

    public async addRating(id: string, rating: number) {
        const user = await this.findOne(id);
        if (!user) {
            return;
        }
        user.ratings = user.ratings || 0;
        user.rating = user.rating || 0;
        user.rating = ((user.rating * user.ratings) + rating) / (user.ratings + 1);
        user.ratings += 1;

        this.updateUser(id, user);
    }
}

export const userService: UserService = new UserService();