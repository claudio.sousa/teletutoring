import { Request, Response, Router } from 'express';
import { IUser, userService } from '.';

const router: Router = Router();
// tslint:disable-next-line
const defaultImage = 'iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAFadJREFUeJztnV1sW2Wax//ZDxiN4zojLYkwGlz3AsM2caWth3VXuKoTVlMttXu1qknCVSmGKbRs1S3CQWSCSFg6VZZ0WsCkvVjlo+nFXtQuq462SboJ2mRoutrYhY25yEelGiWjkeI4RpqZi7MX5gTHObbPsc857/l4flcQp85zPv7v8/G+7/MCBEEQBEEQhMzUsTbALKxvbHKJ1BIAYCW9ipX02rbPNza/w3xqcdvPfPubd3yPw94Ih70JAOB2OdGwq56eoYLQzZWR5Yer3IP0Gqbmknjw7e+wkl7Fg/TaDjHIjdvlhM1qwT7XHvz08cewz7WHxCMTdAOrZH1jk5uaSyKRWsL0vftIpJaQyeZYm7UNm9UCt8sJ3/5mHPS0kGiqgG6WSHhB3LzzJabnkop7BaVwu5w46GnBc/v3Ith6gJ5/BegGlWF+YZGbmktiJD4BPn8wGgG/F8/t34uA34vdTzTR+1AE3ZAi5hcWuZH4BOKTs7r1EtXidjnREWglsRRANwH58GkoNm5oTyEVn6cZnd+Lxcx5i2kvHACm7ia54fgEhmPjrE3RLDarBQG/FyfbA9j39B7TvS+mu+D1jU0uPjmLvuiY6UKoWuG9yktHnzfNe2OaC13f2OQujcRweTSuuXKs3nDYGxEJh0wRfhn64gAShpLYrBacbA/g9Y6gYYViyIsCSBhqwgvlndfaDfc+Ge6CAOD9T0Y5Eob68KGXkXIUw1wIAMQmZri3Llyl5JsxPk8zusIv4uDPWnT/fun+AoD8IsFw9wCm5+6zNoUooDPYhvNnj+s6P9Gt4TwUTmkbm9WC82eP6zbs0qXRQH5JSLh7gGa+dYLP04xoz2ndLWHRlbE8738yyvVFx1ibQUjEZrUgEg7hjc6junnvdGMokM81Qmf6yGvonIDfi2jPKV3kJpo3kGfoxm3u3IWrlGsYBJvVguv9Ec1XujRtHM8r7w5wtKDQmETCIU1PMGrWMIBCKrOg5ZDrL1gbUIr5hUXuQOhNCqlMQH5z2irmFxY5rS2p15QxPEM3bnPh7ouszSBURot5iWYM4Tn3qyvcpZEYazMIhkR7TmlmYlETRvBQMk7waCV5Z24AkF+aHu6+iPjkLGtTCA3RGWzDZ++dZvqOMhfI+sYmd/hEF1WqCEFYi4SpQEgchBhYioSZQEgchBRYiYSJQEgcRDWwEAkTgXiPneZIHEQ1qC2SP1PrD/G88u4AiYOomuHYOM796gqn1t9TVSA0z0HIwaWRGIZu3FZFJKoJ5NfDN0gchGyEuy8iNjGjuEhUieViEzNc6MwHavwpwkTYrBbcGuxVtGew4gKZX1jkDp/oolW5hCI47I2YGftIsaXyii53X9/YpCXrhKKspNdw7EyfYt+vaA4S7r5ITdwIxZmeu69YZUsxgbz/yShHiw8Jtbg0ElMkaVckbpu6m+QOn+hS4qsJoiQ2qwUzYx/J2ntL9hyEzzsIQm0y2RzC3QOyfqfsHuTYP/VRaFUCm9WCl4JteG7/XjRY67d+7vM0I5PNIZFawnxqEclvlnV91DRr5NxsJatAaL5DGP5YgM5gm6R/t5Jew/RcEtP3viLBSGRm7CNZ5kdkE8j6xib3zAsnqKRbAN+4WaowSpFILWFqLomPR+Mklgq4XU7MXh/QjkAotNrO6x1BRMIh2KwWRb4/kVqCWc9zF4scoZYsAqGq1Q/wrWt8nmbV/iaJpTRffz5YU1WrZoHwVSt6MPlk+3p/RDGvIYbh2DiG4xN0mND3+DzN+M2Vvqrf85onCi+NxEgcyG/kuTXYy1QchXZEe07BYW9kaosWmJ67X9MEYk0eZPnhKq21Qv6ljPacYm3GDjLZHPqiYzB7I75aFjT+eS1/+Lv6n/7ybjJVy1foHq2KAwB+9Ogj+Pu/+xsc9LQgfue3+MMf/8TaJCZksjk8+shfYuLzf++R+m+rDrGm7iZNvwFKy+IoxOdpxq3BXrhdTtamMOPyaBzrG5uSQ62qPUia+8kvH5g493C7nPi3fzmLHz36CGtTRNH0Vz/BP/7ch//87//B6u/XWZujOn/445+w+vt13PuvW5K8SFUeZOpukjNzlcRmtSDac5p5Qi4VvdotF8OxcSw/XJXkRaoSSG/0WjX/zDCcbA/oNlxxu5y43h9hbQYzpB7+KlkgZvceDnsjul59kbUZNeHzNOP1jiBrM5gg1YtIFojZvUckHGJtgiwouQxG60jxIpIEQt6jUbaFh6yxWS042R5gbQYThmPjoitakgQyHJ+oziKDEPB7WZsgK2YNswCInjwVLZDlh6umn/foCLSyNkFWbFaL4UQvlsujcVG/J1ogH4v8QqNis1p0W7kqx5FDz7I2gQmZbE5U+1LRAhkyufcwojgAwOdpYW0CM8R4EVECGbpxmzP7gkSHvYm1CYrgsDeatpqVSC1hfmGxrBcRJRCzJ+cA8OTjj7E2QTGM6h3FUMmLVBTI8sNVU5d2CWNTaZt4RYHQPnPj49uv3vZgrZHJ5spuqKooELNXrwjjMxKfLPlZWYHMLyxytJ2WMDrxydmSM+tlBULhFWEWpuaSgj8vK5Cbd36riDGEtkh+s8zaBObcvPOl4M9LCmT54SqdRlvA9D3jVvLWs5usTWBOqWippECmS7gcwniYfRIYyN+DqbvJHXlISYGUcjlmxcj77ylSyCOUh5QUSKmkxawYtZpH4vgBoZxbUCDzC4umX3slhBFFkkgtsjZBMyRSSzvKvYICIe8hjBHDLKpgbaf43RcUyBf3vlLFGL0xb8DRlgbD7RSHnORBJGDEsJNykO0Ul/N3CGT54SrlHyWgbunGp3jl+g6BUNImTGewzTAdTQoxc+OGUhRuohIQCLlcIc6fPc7aBEUwSp8vOVlJr2799w6BUFVjJz5Ps2G3pRr1umqh0ElQiEUQRRQm6jsEYsTJsFqhooW5KHze2wRSqcODWTFyXkYD4k5Khlg0UpbGqCIx4uoAOeA7wG8TCE0QlsaouRk9c2H4gaPmY6DNwrRBl98YeSNYLfCl3m0CoRJvaYy6gYx6ngmzIuRBaOtlaVbSa4ZLaKkpR2UoxJLAWxeusjZBVsQeAWBG+NBzm0CMWqmRi/jkLIxyRkrvp9covBIBlXklcs4AXiSTzUk+7dWsUIglkUw2p/tchCIF8ZBAqkDvcyI091EZPvwkgVSB3rck09yHeEggVaDnETiTzVFyLgESSBUkUku6zUP0LG4WkECqRK8z6+XOwiB2QgKpEj1OsmWyOZo9lwgJpEr0GGZdGomxNkF3kEBqQE+TbZlsTpdejzUkkBqIT87qZvXBUGxcN7ZqAb6ZBQmkBjLZnC7CFlpaIh3+7HgSSI1cHo1rPhe5NBIj71El2wRCrTWlo/XReSW9pmn7tM42gTxJAqmK4di4Zmenw90DrE3QJQ3WegAUYslGuHtAc2HMpZGYZoWrdVqe2g1gR4jVxMIWQ7CSXtPUXpFEaslwOyBZsD3EevwxVnYYguHYuCZ2HGayOYTO9LE2Q9cIVrGokXHtnLtwlemGpEw2h8MnujRfWdM6gjnIPtceJsYYCdYvKGuBGgVBD0JVLHngQxy1k/Zw90VNhHhGoGFXfR1QJJDdTzTVsTHHeCRSS6p5Et5rkTjkgfcegECZt/BDojYSqSUcCL2paMizkl7D4RNdVM6VkcJq7g6BUKlXXpQc3eOTs4oL0IzwcyCAgEAKPySkUaoKmMnmEO6+iNCZD2TJS1bSawid+UC27yO2UzbEOuhpUdUYo2CzWnBrsBczYx+VFEp8chbPvHACvZ9eq+rFzmRz6P30Gg6E3iy7M7Az2EYl+xpwF1RzdyTl6xubnP1gu6oG6R1eHPzIk0gtIXSmr2yCbrNa8FKwDR2B1op5X3xyFjfvfFkxTLNZLYj2nELA70UitYRw9wCFXxKxWS34dvrali4Eq1bP/MPLHE00iaNYHDyZbA7HzvSJSp4d9ka4XXu2hbcbm99hPrUoOvl2u5wY649sW5HNh3a0D108Ab8X1/81Ul4gr7w7wFHJsDI+TzOiPafLbhPo/fSa4svNI+EQul59seTn03P30RulZtViiIRDeOe19i1dCK7m9e3fq55FOoQPZW4N9lbcQ9P16ov4+vNB+DzNstvh8zTj688Hy4qD/73r/RFEwiHZbTAaxTm4oAeZX1jkDoTeVMUgPWGzWnCyPYDXO4JVJcFyjeQBvxcn2wNViY7fQEURgjDf/W9smyZKzpxTHvIDtQqjmERqCSPxCcQnZ0XPtLtdTnQEWhHwe2XZ+ckLRU+NJ5TG52nGb670iRMI5SH55Lkj0CqbMIRIpJaQSC1iJb2GB9/+DivpVTRY67cS9oOeFrhdTkXLtsOxcdy886Xpk/kPzx7HG51HxQlk6MZtLtx9UXmrNIjP04zOQCs6g22sTVEVvvPicHzClAn9zNhH2Pf0HnECMdt8iMPeiIDfi1+0B6h5BfIh2HBsHCPxCVPsLXHYG/F//3Flhx7Krt79+csRzsgjic1qQcDvxZFDzyLg97I2R7MMx8YN71U6g2347L3T0gTy6+EbnNH2NZMoqieRWsLl0bghK2Bj/W8j2HpAmkCWH65yf/3CCeWsUpGA34uOgJ9EIQN8rnJ5NG6IpSzFy0sKqbhBynvsNKfnmxDwe/Hh2eOUVyiEEWbpS4VXgIi+WCfbA/JbpAIOeyNuDfZirP9tEoeC+DzNuDXYiw/PHtftCuLOQGvJzyp6ED1WswJ+L6I9p3T7wPRKfp9Kn67CrlLVK56KHqRhV32dnuYDOoNtGOt/m8TBAN5r6ynP6yjjPQCRrUePHHpWFmOUhvccBDv4hZx66W1QafAXJZBg64E6rcfx/IMh2JN/FqdZm1GRgN9bsZOP6ObVv9B4sh4Jhyis0hBul1PzS3U6Av6KvyO6D9b6xib3zAsnNLny02FvxNefD7I2gygik81By+9MueScR7QHadhVX6fV5Is2AmkTft+9FhH7zkg6H0SLL6LD3qh5V25mtBia88uNxCBJILufaNJcybdSmY5gixYHsJPtga3eu5WQfMKU1ryI1m4+sZNyM9VqY7Na8HpHUPTvSxaIlryIXNtPCWXxeZo185ykeA+gyjMKteJF9DKBSUATs+tSvQdQpUC04EVsVguFVzpCC7miVO8B1HDKLWsvQj2E9YXb5WQaZjnsjZK9B1CDQHY/0VTHUiQUXukPlmFWJByS7D2AGs9JV7IdTiW0ENMS0jhy6G+Z/F23y4mXjj5f1elpNQmkYVd93fmzx2v5iqpQuk8UoQxKtF8Vw/mzL1f9b2sSCAC8dPT5OrUvnPIP/aK25+8MtuHgz1qqPnuzZoEAtSm0Gp6j5tq6Rc1nZ7NaUGuEI4tA9j29R9WEnTyIftlXcHqT0kR7TlWVmBcii0AA4J3X2uvU2EVG+Ye+USscD/i9gn2upCKbQACosouMvIf+UVokcu4ulVUgaoRadAqv/lE6zJIjtOKRVSCA8qGWW8UYllAGJQe5zmCbLKEVj+wCAYCx/ohieYJeumUQpVFqkHPYG2uuWhWjiEB2P9FUp0SHEVYTTYS8KDXIjfVHZAuteBQRCJBvFVTN4rByOOxNsn4fwQ65B7toz6kdh9/IgWICAYDz//yyrLPsTz7+mGzfRbBFzsGuM9hW9VqrSigqEAC4XnS4fS1Qidc4yDXYuV3Okp3Z5UBxgTTsqq+TK2mnCULjIMdgx/cCVhLFBQLk50fkSNqpgmUcah3sbFaLIkl5MaoIBMgn7bWIhMRhLGp9ntf7I4ok5cWoJhAgvzS+2pl2Cq+MR7W5abTnVE1L2KWgqkCA/Ex7Nc0W1FwFSqjDk1UIJNpzSrGKlRCqCwQAPnvvtGSR7Kr/sULWEKyQWupVWxwAI4EA0kVCOYjxkFLqZSEOgKFAAGkiabDWK2wNoTZi80pW4gAYCwQQLxJK0o2HmLySpTgADQgEyIuk0rotCrHMB2txABoRCJBft1VunmQ4Nq6iNYQaDMcnBH+enwR8m7k4AAlHsKnF0I3b3LkLVwWP7eoMtuG8jg+sJ/JksjkcO9OH6bn7Oz6zWS24NdiryiSgGDRhRDHzC4vc4RNdgiJxu5yI9pymkEunTM/dR7h7ACvptR2fuV1OjPVHKp48qyaaMaSY9Y1N7vCJLiRSSzs+4/sdUXd3fdH76TX0RccEP+PPuFd6bZVUNGVMMesbm9y5C1dL5h/8TaWQS9uspNcQ7h4QDKmAfGPpd15r1+S7qEmjiimXl/AtXqiZtTa5NBJDX3Ss5LO73h9RbV1VNWjWsGLmFxa5cPeAYMgF5L3Jh2ePa+aoL7OTSC3h3IUrJb2Gz9OMaM9pTeUbQmjauGLWNza5vugYLo3EBD+3WS2IhENVHZRCyEMmm9vyGqXQckhVjC6MLCY2McOFuy8Kum0gXw05f/Zl6oKiMsOxcfRFxwQrVMAPFUitlHDFoBtDi1nf2OTC3RcRn5wt+Tu8G6ewS1mm5+6jN3qtZDgF6MtrFKI7g4uZupvkStXVeTqDbYiEQyQUmalUnQL06TUK0aXRxaxvbHKV4l6AhCIXK+k19EXHyi7/4fPBNzqP6vod07XxxSw/XOXeunC1bNgFkFCqRUwoBeTPrqz20EytofsLEGLqbpIT8yB9nmacbA/QHEoFhmPjuDwaL1li59FL6VYKhrkQIWITM9xbF66WzU+AfPOAjkArOoNt5FW+ZyW9ho9H4xiKjZesFvL4PM3oCr+o6Qm/ajHcBQkxdOM2V678WEjA78WRQ88i4PeabglLJptDfHJWlLcAjC0MHsNemBBiQy8gn2QG/F749u81tFh4Udy882XF3I0n4PfiZHvA0MLgMfwFCjG/sMhdHo1L2oQV8Hvx3Pdi0XsYlkgtYWouiZH4hChPAfwwYETCIUPlGJUwzYUKsb6xyQ3FxvHxaFxU+MXjsDfC52mBb/9euF17NL83hRfEF/e+wtRcsmJOUYjb5dwqZBihKiUV011wKabuJrnh+ATik7OSXiAgP7q6XU749jfD7XLCYW9iJprpuftYSa8i+c0y5lOLosLJYhz2RgT8XnQEWnU7wScXpr74UsQmZjg+JpcqlkL4I6v3ufZgV/2P4XY5t9oXVbNOLJPNbYVEK+lVrKTXsLH5HeZTi3iQXpPkBYvhQ6gjh56V9Yw/vUM3ogKxiRnui3tfIT45W9MLKAaHvXGrHWcitVSTOMXgdjlx0NNCnqIMdFMksPxwlZueS2L63leYnksqLhi5cbuccLv2bFXmzJhTSIVuUA0sP1zlEqlFJFJLmL53X5VRXywOeyPcrj1oeWo3Dnpa8uEdCUIydMNkZn1jk0ukljCfWkQmm8P0vXySrIR4+OJAg7UeLU/thsPeCIe9yRTzE2pBN5IBU3eTXOH/82ISgk/yCyEBqMf/Ax/uJxwN8xXxAAAAAElFTkSuQmCC';

router.put('/:userId', async (req: Request, res: Response) => {
    res.send({
        data: await userService.updateUser(req.params.userId as string, req.body as IUser)
    });
});

router.get('/me', async (req: Request, res: Response) => {
    const session = req.session as any;
    res.send({
        data: await userService.findOne(session.userSession.userId)
    });
});

router.get('/:userId/photo', async (req: Request, res: Response) => {
    const b64photo = (await userService.getPhoto(req.params.userId as string)) || defaultImage;
    res.contentType('image/jpeg');
    res.send(new Buffer(b64photo, 'base64'));
});

router.get('/children', async (req: Request, res: Response) => {
    if (!req.session) {
        return res.status(500).send('No session found');
    }

    res.send({
        data: await userService.findChildren(req.session.userSession.userId as string)
    });
});

router.get('/:userId', async (req: Request, res: Response) => {
    res.send({
        data: await userService.findOne(req.params.userId as string)
    });
});

router.get('/', async (req: Request, res: Response) => {
    res.send({
        data: await userService.getUsers()
    });
});

export const userController: Router = router;
