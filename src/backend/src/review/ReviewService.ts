import shortid from 'shortid';
import { mongoDB } from '../db/MongoDB';
import { userService } from '../user';

export interface IReview {
    id: string;
    rating: number;
    comment: string;
}

class ReviewService {

    public async insertReview(userId: string, review: IReview) {
        const reviews = await mongoDB.reviewsCollection;
        reviews
            .insert(({ ...review, id: shortid(), userId }));

        userService.addRating(userId, review.rating);
    }

}

export const reviewService: ReviewService = new ReviewService();