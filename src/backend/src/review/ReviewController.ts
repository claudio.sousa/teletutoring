import { Request, Response, Router } from 'express';
import { IReview, reviewService} from './ReviewService';

const router: Router = Router();

router.post('/:userId', async (req: Request, res: Response) => {
    res.send({
        data: await reviewService.insertReview(req.params.userId as string, req.body as IReview)
    });
});

export const reviewController: Router = router;
