
import { Collection, Db, MongoClient } from 'mongodb';

class MongoDB {

    private static DEFAULT_MONGO_HOST = '127.0.0.1:27017';
    private static DEFAULT_MONGO_DB = 'test';

    private static get MONGO_CONNECTION_STRING() {
        const host = process.env.MONGO_HOST || MongoDB.DEFAULT_MONGO_HOST;
        const db = process.env.MONGO_DB || MongoDB.DEFAULT_MONGO_DB;
        return `mongodb://${host}/${db}`;
    }

    private _db: Promise<Db> | null = null;

    private get DB() {
        if (this._db == null) {
            this._db = MongoClient.connect(MongoDB.MONGO_CONNECTION_STRING).then((client) => client.db('teletutoring'));
        }
        return this._db;
    }

    public get usersCollection() {
        return this.DB.then((db) => db.collection('users'));
    }

    public get photosCollection() {
        return this.DB.then((db) => db.collection('photos'));
    }

    public get credentialsCollection(): Promise<Collection> {
        return this.DB.then((db) => db.collection('credentials'));
    }

    public get sessionsCollection() {
        return this.DB.then((db) => db.collection('sessions'));
    }

    public get reviewsCollection() {
        return this.DB.then((db) => db.collection('reviews'));
    }

    public get transactionsCollection() {
        return this.DB.then((db) => db.collection('transactions'));
    }

    public async flush() {
        const db = await this.DB;
        db.executeDbAdminCommand({ fsync: 1, lock: true });
    }

}

export const mongoDB: MongoDB = new MongoDB();