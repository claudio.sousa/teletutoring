
import { expect } from 'chai';
import { lookupService } from '../src/lookup/LookupService';

describe('LookupService', () => {
    it('should retrieve subjects', () => {
        expect(lookupService.subjects()).deep.equals([
            'MATH',
            'FRANCAIS',
            'ANGLAIS',
            'ALLEMAND',
            'SCIENCES_NATURE',
            'HISTOIRE',
            'GEOGRAPHIE',
            'CITOYENNETE',
            'ARTS_MANUELLES',
            'ARTS_VISUELS',
            'MUSIQUE',
            'EDUCATION_PHYSIQUE'
        ]);
    });

    it('should retrieve subjects', () => {
        expect(lookupService.subjects()).deep.equals([
            'MATH',
            'FRANCAIS',
            'ANGLAIS',
            'ALLEMAND',
            'SCIENCES_NATURE',
            'HISTOIRE',
            'GEOGRAPHIE',
            'CITOYENNETE',
            'ARTS_MANUELLES',
            'ARTS_VISUELS',
            'MUSIQUE',
            'EDUCATION_PHYSIQUE'
        ]);
    });

    it('should retrieve levels', () => {
        expect(lookupService.levels()).deep.equals(
            [
                '1P',
                '2P',
                '3P',
                '4P',
                '5P',
                '6P',
                '7P',
                '8P',
                '9P',
                '10P',
                '11P'
            ]
        );
    });

    it('should retrieve languages', () => {
        expect(lookupService.languages()).deep.equals([
            'FR',
            'EN',
            'DE',
            'IT'
        ]
        );
    });
});
