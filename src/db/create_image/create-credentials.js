db.credentials.insertMany([
    {
        "userId": "KS8XOlOrI",
        "username": "tutor",
        "password": "$2b$10$aL8fG3MR3ZRXczxGp0E27.NHfhb55kFcODw/LUqt1DVveY1Jk0YkO"
    },
    {
        "userId": "XS7XOlOrp",
        "username": "tutor2",
        "password": "$2b$10$h9XxmJ8LEw2T7yIHf5MTSewFLkhe9ambjCMRL2k9G4nglYZgl8vXm"
    },
    {
        "userId": "LAUDZs98dk",
        "username": "parent",
        "password": "$2b$10$kfhHoNzBnzagVpg4vKJvIevHkzk2ZcvwLxN9rKuYMXsTMicVg7qIm"
    },
    {
        "userId": "2DY7UdEvdm",
        "username": "student",
        "password": "$2b$10$j6EJvHrz/UXLAsUu6fTmj.Fg53RYTPzS.B30XyeS0ycmWjwngEeqG"
    }
]);