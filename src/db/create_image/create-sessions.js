db.sessions.insertMany([
    {
      "id": "KS8XOlOrI2",
      "date": 1550520432595,
      "price": 20,
      "tutoredClass": {
        "subject": "FRANCAIS",
        "levels": [],
        "price": 20
      },
      "duration": 2,
      "tutorId": "KS8XOlOrI",
      "learnerId": "2DY7UdEvdm",
      "status": 3
    },
    {
      "id": "KS8XOlOrI23",
      "date": 1550693232595,
      "tutoredClass": {
        "subject": "ALLEMAND",
        "levels": [],
        "price": 25
      },
      "duration": 2,
      "tutorId": "KS8XOlOrI",
      "learnerId": "2DY7UdEvdm",
      "status": 1
    },
    {
      "id": "KS8XOlOrI24",
      "date": 1551211632595,
      "tutoredClass": {
        "subject": "CITOYENNETE",
        "levels": [],
        "price": 25
      },
      "duration": 5,
      "tutorId": "KS8XOlOrI",
      "learnerId": "2DY7UdEvdm",
      "status": 1
    },
    {
      "id": "KS8XOlOrI25",
      "date": 1551211632595,
      "tutoredClass": {
        "subject": "MUSIQUE",
        "levels": [],
        "price": 50
      },
      "duration": 1,
      "tutorId": "KS8XOlOrI",
      "learnerId": "2DY7UdEvdm",
      "status": 1
    },
    {
      "id": "KS8XOlOrI26",
      "date": 1551298032595,
      "tutoredClass": {
        "subject": "FRANCAIS",
        "levels": [],
        "price": 25
      },
      "duration": 3,
      "tutorId": "KS8XOlOrI",
      "learnerId": "KS8XOlOrI",
      "status": 1
    },
    {
      "id": "eIVtfK2aj",
      "date": 1551117271381,
      "tutoredClass": {
        "subject": "MATH",
        "levels": [],
        "price": 40
      },
      "duration": 2,
      "tutorId": "KS8XOlOrI",
      "learnerId": "2DY7UdEvdm",
      "status": 1
    },
    {
      "id": "f4MGTQ_7E",
      "date": 1551646519339,
      "tutoredClass": {
        "subject": "HISTOIRE",
        "levels": [],
        "price": 15
      },
      "duration": 3,
      "tutorId": "KS8XOlOrI",
      "learnerId": "KS8XOlOrI",
      "status": 1
    },
    {
      "id": "Og4RzjCiV",
      "date": 1551717466637,
      "tutoredClass": {
        "subject": "MATH",
        "levels": [],
        "price": 22.45
      },
      "duration": 4,
      "tutorId": "XS7XOlOrp",
      "learnerId": "XS7XOlOrp",
      "status": 1
    },
    {
      "id": "TQLy7SBmZ",
      "date": 1552324293588,
      "tutoredClass": {
        "subject": "MATH",
        "levels": [
          "2P",
          "3P"
        ],
        "price": 15
      },
      "duration": 1,
      "tutorId": "XS7XOlOrp",
      "learnerId": "2DY7UdEvdm",
      "status": 1
    },
    {
      "id": "agcfXap5H",
      "date": 1553531400407,
      "tutoredClass": {
        "subject": "MATH",
        "levels": [
          "2P",
          "3P"
        ],
        "price": 15
      },
      "duration": 1,
      "tutorId": "XS7XOlOrp",
      "learnerId": "2DY7UdEvdm",
      "status": 3
    }
  ]);