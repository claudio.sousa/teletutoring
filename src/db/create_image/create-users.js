db.users.insertMany([
    {
        "id": "KS8XOlOrI",
        "firstName": "Stéphane",
        "lastName": "Malandain",
        "email": "stephane.malandain@hesge.ch",
        "lastConnexion": 1550694778944,
        "languages": [
            "FR",
            "EN"
        ],
        "description": "Professor web and mobile technology",
        "verified": true,
        "availableNow": false,
        "rating": 4,
        "type": 0,
        "classes": [
            {
                "subject": "MATH",
                "levels": [
                    "1P",
                    "2P"
                ],
                "price": 15
            },
            {
                "subject": "FRANCAIS",
                "levels": [
                    "1P",
                    "2P"
                ],
                "price": 22
            },
            {
                "subject": "ANGLAIS",
                "levels": [
                    "1P",
                    "2P"
                ],
                "price": 3
            }
        ],
        "availabilities": [
            {
                "daysOfWeek": [
                    0,
                    6
                ],
                "startTime": 1551085200,
                "endTime": 1551114000,
                "startRecur": 1551085200,
                "endRecur": 1553533200,
                "id": "pEvYDFbzR"
            }
        ],
        "credit": 80,
        "ratings": 2
    },
    {
        "id": "XS7XOlOrp",
        "firstName": "Yassin",
        "lastName": "Rekik",
        "email": "yassin.rekik@hesge.ch",
        "lastConnexion": 1550695778944,
        "languages": [
            "FR",
            "EN",
            "IT"
        ],
        "description": "I am the greatest teacher of the world.",
        "verified": true,
        "availableNow": true,
        "rating": 4,
        "type": 0,
        "classes": [
            {
                "subject": "MATH",
                "levels": [
                    "2P",
                    "3P"
                ],
                "price": 15
            },
            {
                "subject": "FRANCAIS",
                "levels": [
                    "5P",
                    "6P"
                ],
                "price": 20.45
            }
        ],
        "availabilities": [
            {
                "daysOfWeek": [
                    0,
                    3
                ],
                "startTime": 1551085200,
                "endTime": 1551114000,
                "startRecur": 1551085200,
                "endRecur": 1553533200,
                "id": "pEvYDFbzR"
            }
        ],
        "credit": 80,
        "ratings": 2
    },
    {
        "id": "XS7XOlOri",
        "firstName": "Nabil",
        "lastName": "Abdennadher",
        "email": "nabil.abdennadher@hesge.ch",
        "lastConnexion": 1550695778944,
        "languages": [
            "FR",
            "EN"
        ],
        "description": "Responsable de l'institut inIT",
        "verified": true,
        "availableNow": true,
        "rating": 4.9,
        "type": 0,
        "classes": [
            {
                "subject": "MATH",
                "levels": [
                    "2P",
                    "3P",
                    "4P",
                ],
                "price": 75
            },
            {
                "subject": "FRANCAIS",
                "levels": [
                    "4P",
                    "5P",
                    "6P"
                ],
                "price": 90.45
            }
        ],
        "availabilities": [
            {
                "daysOfWeek": [
                    0,
                    3
                ],
                "startTime": 1551085200,
                "endTime": 1551114000,
                "startRecur": 1551085200,
                "endRecur": 1553533200,
                "id": "pEvYDFbzR"
            }
        ],
        "credit": 80,
        "ratings": 2
    },
    {
        "id": "LAUDZs98dk",
        "firstName": "Dimitri",
        "lastName": "Lizzi",
        "email": "dimitri@lizzi.ch",
        "lastConnexion": 1550695778944,
        "languages": [
            "FR",
            "EN",
            "IT"
        ],
        "description": " I am the greatest teacher of the world.",
        "rating": 4.4,
        "type": 2,
        "credit": 85
    },
    {
        "id": "2DY7UdEvdm",
        "parentId": "LAUDZs98dk",
        "firstName": "Bruno",
        "lastName": "da Silva",
        "email": "bruno.silva@hotmail.com",
        "lastConnexion": 1550694778944,
        "languages": [
            "EN"
        ],
        "description": "tall",
        "rating": 3.3,
        "type": 1
    },
    {
        "id": "2DY7UdEvdk",
        "parentId": "LAUDZs98dk",
        "firstName": "George",
        "lastName": "Washington",
        "email": "george.washington@hotmail.com",
        "lastConnexion": 1550694778944,
        "languages": [
            "EN"
        ],
        "description": "Political leader",
        "rating": 5.0,
        "type": 1
    },
    {
        "id": "2DY7UdEvdl",
        "parentId": "LAUDZs98dk",
        "firstName": "Donald",
        "lastName": "Trump",
        "email": "donald.trump@hotmail.com",
        "lastConnexion": 1550694778944,
        "languages": [
            "EN"
        ],
        "description": "Fake news and fake president",
        "rating": 0.0,
        "type": 1
    }
]);
