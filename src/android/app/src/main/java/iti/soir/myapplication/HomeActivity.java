package iti.soir.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.DataResult;
import iti.soir.myapplication.data.InterfaceAPI;
import iti.soir.myapplication.data.SessionStatusType;
import iti.soir.myapplication.data.User;
import iti.soir.myapplication.session.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends Activity {

    public static TextView textViewWelcome;
    public static TextView textViewUserName;
    public static ImageView imageViewWelcome;
    public static TextView textViewNextSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        textViewWelcome = findViewById(R.id.homeWelcome);
        textViewUserName = findViewById(R.id.homeUserName);
        imageViewWelcome = findViewById(R.id.homeImageWelcome);
        textViewNextSession = findViewById(R.id.homeNextSessionDescription);

        User loggedInUser = BackendService.getLoggedInUser();
        textViewUserName.setText(loggedInUser.firstName + " " + loggedInUser.lastName);

        textViewWelcome.setText("Welcome !");

        try {
            InterfaceAPI interfaceAPI = BackendService.getInterfaceAPI();
            Call<DataResult<List<Session>>> call2 = interfaceAPI.getSessions();
            call2.enqueue(new Callback<DataResult<List<Session>>>() {
                @Override
                public void onResponse(Call<DataResult<List<Session>>> call, Response<DataResult<List<Session>>> response) {
                    if (!response.isSuccessful()) {
                        textViewUserName.setText("Code :" + response.code());
                        return;
                    }

                    List<Session> sessions = response.body().getData();
                    if (!sessions.isEmpty()) {
                        Session nextSession = sessions.get(0);
                        for (int i = 1; i < sessions.size(); ++i) {
                            Session currentSession = sessions.get(i);
                            if ((currentSession.date > nextSession.date) && (currentSession.status == SessionStatusType.confirmed)) {
                                nextSession = currentSession;
                            }
                        }
                        
                        String content = "";
                        content += new SimpleDateFormat("dd MMM - HH:mm").format(new Date(nextSession.date)) + "\n";
                        content += nextSession.tutoredClass.subject + "\n";
                        content += nextSession.tutoredClass.price + " CHF/Hour" + "\n";
                        content += nextSession.duration + " hours" + "\n";
                        textViewNextSession.setText(content);
                    }
                }

                @Override
                public void onFailure(Call<DataResult<List<Session>>> call, Throwable t) {
                    textViewNextSession.setText(t.getMessage());
                }
            });
        } catch (BackendService.BackendServiceNotInitializedException e) {
            finish();
            return;
        }
    }
}
