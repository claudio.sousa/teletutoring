package iti.soir.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.DataResult;
import iti.soir.myapplication.data.InterfaceAPI;
import iti.soir.myapplication.data.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends Activity {

    public static TextView textViewName;
    public static TextView textViewEmail;
    public static TextView textViewDescription;
    public static TextView textViewVerified;
    public static TextView textViewRating;
    public static TextView textViewClasses;
    public static TextView textViewLanguages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        textViewName = findViewById(R.id.profileName);
        textViewEmail = findViewById(R.id.profileEmail);
        textViewDescription = findViewById(R.id.profileDescription);
        textViewVerified = findViewById(R.id.profileVerified);
        textViewRating = findViewById(R.id.profileRating);
        textViewClasses = findViewById(R.id.profileClasses);
        textViewLanguages = findViewById(R.id.profileLanguages);

        User loggedInUser = BackendService.getLoggedInUser();
        textViewName.setText(loggedInUser.firstName+" "+loggedInUser.lastName);
        textViewEmail.setText(loggedInUser.email);
        textViewRating.setText("Rating: "+Float.toString(loggedInUser.rating));
        textViewDescription.setText("\""+loggedInUser.description+"\"");

        if(loggedInUser.verified) {
            textViewVerified.setText("User verified");
            textViewVerified.setTextColor(getResources().getColor(R.color.colorGreen));
        }
        else {
            textViewVerified.setText("Get verified status !");
            textViewVerified.setTextColor(getResources().getColor(R.color.colorRed));
        }

        String languages = "";
        for (int i = 0; i < loggedInUser.languages.size(); i++) {
            if(i == loggedInUser.languages.size()-1)
                languages += loggedInUser.languages.get(i);
            else
                languages += loggedInUser.languages.get(i)+", ";
        }
        textViewLanguages.setText(languages);

        if (loggedInUser.classes != null) {
            String classes = "";
            for (int i = 0; i < loggedInUser.classes.size(); i++) {
                classes += loggedInUser.classes.get(i).subject + "\n";
                if (loggedInUser.classes.get(i).levels != null) {
                    for (int j = 0; j < loggedInUser.classes.get(i).levels.size(); j++) {
                        if (j == loggedInUser.classes.get(i).levels.size() - 1)
                            classes += loggedInUser.classes.get(i).levels.get(j);
                        else
                            classes += loggedInUser.classes.get(i).levels.get(j) + ", ";
                    }
                }
                classes += "\n" + loggedInUser.classes.get(i).price + " CHF" + "\n";
                classes += "\n";
            }
            textViewClasses.setText(classes);
        } else {
            findViewById(R.id.titleClassText).setVisibility(View.INVISIBLE);
            findViewById(R.id.tabClassText).setVisibility(View.INVISIBLE);
        }

        Context context = getApplicationContext();
        int picture_id = context.getResources().getIdentifier("picture_" + loggedInUser.id.toLowerCase(), "drawable", context.getPackageName());
        if (picture_id != 0) {
            ((ImageView) findViewById(R.id.profilePicture)).setImageResource(picture_id);
        } else {
            ((ImageView) findViewById(R.id.profilePicture)).setImageResource(R.drawable.profilepicture1);
        }
    }
}
