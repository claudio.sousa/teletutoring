package iti.soir.myapplication.data;

import com.google.gson.annotations.SerializedName;

public class DataResult<TData> {
    @SerializedName("data")
    private TData data;

    public DataResult() {

    }

    public DataResult(TData data) {
        this.data = data;
    }

    public TData getData() {
        return data;
    }

    public void setData(TData data) {
        this.data = data;
    }
}