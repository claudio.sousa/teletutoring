package iti.soir.myapplication.session;

import java.util.List;

import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.DataResult;
import iti.soir.myapplication.data.InterfaceAPI;
import retrofit2.Call;
import retrofit2.Callback;

public class SessionService {
    private InterfaceAPI interfaceAPI;

    public SessionService() throws BackendService.BackendServiceNotInitializedException {
        interfaceAPI = BackendService.getInterfaceAPI();
    }

    public void getSessions(Callback<DataResult<List<Session>>> callback) {
        Call<DataResult<List<Session>>> call = interfaceAPI.getSessions();
        call.enqueue(callback);
    }
}
