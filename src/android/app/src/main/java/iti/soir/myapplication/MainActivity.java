package iti.soir.myapplication;

import android.app.LocalActivityManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost host = (TabHost)findViewById(R.id.tabHost);
        LocalActivityManager mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState);
        host.setup(mLocalActivityManager );
        host.setup(mLocalActivityManager);

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("Accueil");
        spec.setContent(new Intent(this, HomeActivity.class));
        spec.setIndicator("", getResources().getDrawable(R.drawable.home));
        host.addTab(spec);

        //Tab 2
        spec = host.newTabSpec("Sessions");
       // spec.setContent(R.id.tab2);
        spec.setContent(new Intent(this, SessionsActivity.class));
        spec.setIndicator("", getResources().getDrawable(R.drawable.sessions));
        host.addTab(spec);

        //Tab 3
        spec = host.newTabSpec("Tutors");
        spec.setContent(new Intent(this, TutorsActivity.class));
        spec.setIndicator("", getResources().getDrawable(R.drawable.search));
        host.addTab(spec);

        //Tab 4
        spec = host.newTabSpec("Profile");
        spec.setIndicator("", getResources().getDrawable(R.drawable.profile));
        spec.setContent(new Intent(this, ProfileActivity.class));
        host.addTab(spec);
    }
}
