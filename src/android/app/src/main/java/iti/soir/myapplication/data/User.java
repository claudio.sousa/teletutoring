package iti.soir.myapplication.data;

import java.util.ArrayList;

public class User {
    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public int lastConnection;
    public ArrayList<String> languages;
    public String description;
    public boolean verified;
    public boolean availableNow;
    public float rating;
    public int type;
    public ArrayList<Classes> classes;
    //private ArrayList<String> availabilities;

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
