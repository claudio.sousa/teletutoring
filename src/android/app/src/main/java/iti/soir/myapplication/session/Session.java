package iti.soir.myapplication.session;

import iti.soir.myapplication.data.TutoredClasses;
import iti.soir.myapplication.data.User;

public class Session {
    public String id;
    public long date;
    public double price;
    public long duration;
    public String tutorId;
    public String learnerId;
    public User tutor;
    public User learner;
    public int status;
    public TutoredClasses tutoredClass;
}
