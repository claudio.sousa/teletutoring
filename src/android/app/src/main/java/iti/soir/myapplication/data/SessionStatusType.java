package iti.soir.myapplication.data;

public class SessionStatusType {
    public static int requested=0;
    public static int confirmed=1;
    public static int ongoing=2;
    public static int done=3;
    public static int canceled=4;
    public static int rejected=5;
}