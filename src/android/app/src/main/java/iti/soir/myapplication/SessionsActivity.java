package iti.soir.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.DataResult;
import iti.soir.myapplication.session.Session;
import iti.soir.myapplication.session.SessionAdapter;
import iti.soir.myapplication.session.SessionService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SessionsActivity extends Activity {

    SessionAdapter sessionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);

        RecyclerView recList = findViewById(R.id.sessionList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        sessionAdapter = new SessionAdapter();
        recList.setAdapter(sessionAdapter);

        loadSessions();
    }

    private void loadSessions() {
        SessionService sessionService;
        try {
            sessionService = new SessionService();
        } catch (BackendService.BackendServiceNotInitializedException e) {
            this.finish();
            return;
        }

        sessionService.getSessions(new Callback<DataResult<List<Session>>>() {
            @Override
            public void onResponse(Call<DataResult<List<Session>>> call, Response<DataResult<List<Session>>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Impossible to load sessions: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                List<Session> sessions = response.body().getData();
                sessionAdapter.updateSessions(sessions);
            }

            @Override
            public void onFailure(Call<DataResult<List<Session>>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Impossible to load sessions" , Toast.LENGTH_LONG).show();
            }
        });
    }
}
