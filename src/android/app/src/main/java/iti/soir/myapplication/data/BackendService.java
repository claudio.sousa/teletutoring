package iti.soir.myapplication.data;

import android.content.Context;

import java.io.IOException;

import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BackendService {
    private static BackendService instance;
    private final InterfaceAPI interfaceAPI;
    private static User loggedInUser;

    public static void initialize(Context context, String username, String password, String serverUrl) throws ConnectionErrorException, LoginFailedException {
        if (instance == null) {
            instance = new BackendService(context, username, password, serverUrl);
        }
    }

    private static BackendService getInstance() throws BackendServiceNotInitializedException {
        if (instance == null) {
            throw new BackendServiceNotInitializedException();
        }
        return instance;
    }

    private BackendService(Context context, String username, String password, String serverUrl) throws ConnectionErrorException, LoginFailedException {

        OkHttpClient client = new OkHttpClient();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new AddCookiesInterceptor(context)); // VERY VERY IMPORTANT
        builder.addInterceptor(new ReceivedCookiesInterceptor(context)); // VERY VERY IMPORTANT
        client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serverUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        interfaceAPI = retrofit.create(InterfaceAPI.class);

        try {
            Response<DataResult<User>> response = interfaceAPI.login(new LoginCredentials(username, password)).execute();

            if (response.body() != null) {
                loggedInUser = response.body().getData();
            }

        } catch (IOException e) {
            throw new ConnectionErrorException();
        }

        if (loggedInUser == null) {
            throw new LoginFailedException();
        }
    }

    public static InterfaceAPI getInterfaceAPI() throws BackendServiceNotInitializedException {
        return getInstance().interfaceAPI;
    }

    public static User getLoggedInUser() {
        return loggedInUser;
    }

    public static class BackendServiceNotInitializedException extends Exception {}

    public static class LoginFailedException extends Exception {}

    public static class ConnectionErrorException extends Exception {}
}
