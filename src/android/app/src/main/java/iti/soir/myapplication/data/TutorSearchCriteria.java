package iti.soir.myapplication.data;

public class TutorSearchCriteria {

    public TutorSearchCriteriaFilter filter;

    public TutorSearchCriteria(int price_min, int price_max, String subject, String level) {
        this.filter = new TutorSearchCriteriaFilter(price_min, price_max, subject, level);
    }

}

class TutorSearchCriteriaFilter {

    public int[] price_range;
    public String subject;
    public String level;

    public TutorSearchCriteriaFilter(int price_min, int price_max, String subject, String level) {
        this.price_range = new int[] {price_min, price_max};
        this.subject = subject;
        this.level = level;
    }

}
