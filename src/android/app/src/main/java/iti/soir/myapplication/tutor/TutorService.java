package iti.soir.myapplication.tutor;

import java.util.List;

import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.DataResult;
import iti.soir.myapplication.data.InterfaceAPI;
import iti.soir.myapplication.data.TutorSearchCriteria;
import retrofit2.Call;
import retrofit2.Callback;

public class TutorService {
    private InterfaceAPI interfaceAPI;

    public TutorService() throws BackendService.BackendServiceNotInitializedException {
        this.interfaceAPI = BackendService.getInterfaceAPI();
    }

    public void getTutors(Callback<DataResult<List<Tutor>>> callback) {
        Call<DataResult<List<Tutor>>> call = interfaceAPI.getTutors();
        call.enqueue(callback);
    }

    public void searchTutors(Callback<DataResult<List<Tutor>>> callback, TutorSearchCriteria tutorSearchCriteria) {
        Call<DataResult<List<Tutor>>> call = interfaceAPI.searchTutors(tutorSearchCriteria);
        call.enqueue(callback);
    }
}
