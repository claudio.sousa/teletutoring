package iti.soir.myapplication.data;

public class LoginCredentials {

    public LoginCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String username;
    public String password;
}
