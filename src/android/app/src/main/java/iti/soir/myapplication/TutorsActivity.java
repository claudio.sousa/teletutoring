package iti.soir.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.DataResult;
import iti.soir.myapplication.data.TutorSearchCriteria;
import iti.soir.myapplication.lookup.LookUpService;
import iti.soir.myapplication.tutor.Tutor;
import iti.soir.myapplication.tutor.TutorAdapter;
import iti.soir.myapplication.tutor.TutorService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TutorsActivity extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    TutorAdapter tutorAdapter;
    Spinner spinnerSubjects;
    Spinner spinnerLevel;
    SeekBar seekBar;
    TextView priceDisplayed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutors);

        priceDisplayed = findViewById(R.id.price);

        seekBar = findViewById(R.id.seekBarPrice);
        seekBar.setOnSeekBarChangeListener(this);
        this.onProgressChanged(seekBar, seekBar.getProgress(), false);

        final List<String> subjectList = new ArrayList<>();
        spinnerSubjects = findViewById(R.id.spinnerSubjects);
        final ArrayAdapter<String> adapterSubject = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, subjectList);
        adapterSubject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSubjects.setPrompt("Select your subject");
        spinnerSubjects.setAdapter(adapterSubject);

        final List<String> levelList = new ArrayList<>();
        spinnerLevel = findViewById(R.id.spinnerLevel);
        final ArrayAdapter<String> adapterLevel = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, levelList);
        adapterLevel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setPrompt("Select your level");
        spinnerLevel.setAdapter(adapterLevel);

        RecyclerView recList = findViewById(R.id.tutorsList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        Button searchTutorBtn = findViewById(R.id.searchTutorsBtn);
        searchTutorBtn.setOnClickListener(this);

        tutorAdapter = new TutorAdapter();
        recList.setAdapter(tutorAdapter);

        LookUpService lookUpService;
        try {
            lookUpService = new LookUpService();
        } catch (BackendService.BackendServiceNotInitializedException e) {
            finish();
            return;
        }
        lookUpService.getSubjects(new Callback<DataResult<List<String>>>() {
            @Override
            public void onResponse(Call<DataResult<List<String>>> call, Response<DataResult<List<String>>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Impossible to load tutors: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                List<String> subjects = response.body().getData();
                subjectList.addAll(subjects);
                adapterSubject.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<DataResult<List<String>>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Impossible to load tutors" , Toast.LENGTH_LONG).show();
            }
        });
        lookUpService.getLevels(new Callback<DataResult<List<String>>>() {
            @Override
            public void onResponse(Call<DataResult<List<String>>> call, Response<DataResult<List<String>>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Impossible to load tutors: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                List<String> levels = response.body().getData();
                levelList.addAll(levels);
                adapterLevel.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<DataResult<List<String>>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Impossible to load tutors" , Toast.LENGTH_LONG).show();
            }
        });

        loadTutors();
    }

    private void loadTutors() {
        TutorService tutorService;
        try {
            tutorService = new TutorService();
        } catch (BackendService.BackendServiceNotInitializedException e) {
            finish();
            return;
        }

        tutorService.searchTutors(new Callback<DataResult<List<Tutor>>>() {
            @Override
            public void onResponse(Call<DataResult<List<Tutor>>> call, Response<DataResult<List<Tutor>>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Impossible to load tutors: " + response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                List<Tutor> tutors = response.body().getData();
                tutorAdapter.updateTutors(tutors);
            }

            @Override
            public void onFailure(Call<DataResult<List<Tutor>>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Impossible to load tutors" , Toast.LENGTH_LONG).show();
            }
        }, new TutorSearchCriteria(
                0,
                seekBar.getProgress(),
                (spinnerSubjects.getSelectedItem() != null) ? spinnerSubjects.getSelectedItem().toString() : "",
                (spinnerLevel.getSelectedItem() != null) ? spinnerLevel.getSelectedItem().toString() : ""
            )
        );
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        priceDisplayed.setText(String.valueOf(i) + " CHF/h");
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onClick(View v) {
        this.loadTutors();
    }
}
