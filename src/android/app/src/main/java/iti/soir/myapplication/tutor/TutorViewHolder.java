package iti.soir.myapplication.tutor;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import iti.soir.myapplication.R;

public class TutorViewHolder extends RecyclerView.ViewHolder {
    public TextView vTutorName;
    public TextView vTutorDescription;
    public TextView vTutorRating;
    public ImageView vTutorProfilePicture;
    public TextView vLessonName;
    public TextView vDate;
    public TextView vPrice;

    public TutorViewHolder(View v) {
        super(v);

        vTutorName = v.findViewById(R.id.tutorName);
        vTutorDescription = v.findViewById(R.id.tutorDescription);
        vTutorRating = v.findViewById(R.id.tutorRating);
        vTutorProfilePicture = v.findViewById(R.id.tutorImage);
        //vLessonName = v.findViewById(R.id.lessonName);
        //vDate = v.findViewById(R.id.date);
        //vPrice = v.findViewById(R.id.price);
    }
}
