package iti.soir.myapplication.session;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import iti.soir.myapplication.R;

public class SessionViewHolder extends RecyclerView.ViewHolder {
    public TextView vTutorName;
    public TextView vLessonName;
    public TextView vDate;
    public TextView vPrice;
    public ImageView vSessionProfilePicture;

    public SessionViewHolder(View v) {
        super(v);

        vTutorName = v.findViewById(R.id.tutorName);
        vLessonName = v.findViewById(R.id.lessonName);
        vDate = v.findViewById(R.id.date);
        vPrice = v.findViewById(R.id.price);
        vSessionProfilePicture = v.findViewById(R.id.tutorImage);
    }
}