package iti.soir.myapplication.session;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import iti.soir.myapplication.R;
import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.User;
import iti.soir.myapplication.data.UserType;

public class SessionAdapter extends RecyclerView.Adapter<SessionViewHolder> {

    private List<Session> sessionList = new ArrayList<>();
    private Context context;

    public void updateSessions(List<Session> sessionList) {
        this.sessionList = sessionList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return sessionList.size();
    }

    @Override
    public void onBindViewHolder(SessionViewHolder sessionViewHolder, int i) {
        Session session = sessionList.get(i);
        User loggedInUser = BackendService.getLoggedInUser();
        User displayedUser = (loggedInUser.type == UserType.tutor) ? session.learner : session.tutor;

        sessionViewHolder.vTutorName.setText(displayedUser.getFullName());
        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("fr-CH"));
        sessionViewHolder.vPrice.setText(formatter.format(session.tutoredClass.price));

        Date d = new Date(session.date);
        DateFormat f = new SimpleDateFormat("dd MMM - HH:mm");
        sessionViewHolder.vDate.setText(f.format(d));

        sessionViewHolder.vLessonName.setText(session.tutoredClass.subject);

        int profilePictureId = context.getResources().getIdentifier("picture_" + displayedUser.id.toLowerCase(), "drawable", context.getPackageName());
        if (profilePictureId != 0) {
            sessionViewHolder.vSessionProfilePicture.setImageResource(profilePictureId);
        } else {
            sessionViewHolder.vSessionProfilePicture.setImageResource(R.drawable.profilepicture1);
        }
    }

    @Override
    public SessionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        View itemView = LayoutInflater.
                from(this.context).
                inflate(R.layout.activity_sessions_card, viewGroup, false);

        return new SessionViewHolder(itemView);
    }
}
