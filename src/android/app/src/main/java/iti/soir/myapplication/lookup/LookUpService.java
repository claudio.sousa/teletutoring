package iti.soir.myapplication.lookup;

import java.util.List;

import iti.soir.myapplication.data.BackendService;
import iti.soir.myapplication.data.DataResult;
import iti.soir.myapplication.data.InterfaceAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LookUpService {
    private InterfaceAPI interfaceAPI;

    public LookUpService() throws BackendService.BackendServiceNotInitializedException {
        interfaceAPI = BackendService.getInterfaceAPI();
    }

    public void getLevels(Callback<DataResult<List<String>>> callback) {
        Call<DataResult<List<String>>> call = interfaceAPI.getLevels();
        call.enqueue(callback);
    }
    public void getSubjects(Callback<DataResult<List<String>>> callback) {
        Call<DataResult<List<String>>> call = interfaceAPI.getSubjects();
        call.enqueue(callback);
    }
    public void getLanguages(Callback<DataResult<List<String>>> callback) {
        Call<DataResult<List<String>>> call = interfaceAPI.getLanguages();
        call.enqueue(callback);
    }
}
