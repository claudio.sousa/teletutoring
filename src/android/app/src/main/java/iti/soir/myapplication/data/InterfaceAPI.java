package iti.soir.myapplication.data;

import iti.soir.myapplication.session.Session;
import iti.soir.myapplication.tutor.Tutor;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

import java.util.List;

public interface InterfaceAPI {

    @GET("/sessions")
    Call<DataResult<List<Session>>> getSessions();

    @GET("/tutors")
    Call<DataResult<List<Tutor>>> getTutors();

    @GET("lookups/levels")
    Call<DataResult<List<String>>> getLevels();

    @GET("lookups/subjects")
    Call<DataResult<List<String>>> getSubjects();

    @GET("lookups/languages")
    Call<DataResult<List<String>>> getLanguages();

    @POST("authentication/login")
    Call<DataResult<User>> login(@Body LoginCredentials loginCredentials);

    @POST("tutors/search")
    Call<DataResult<List<Tutor>>> searchTutors(@Body TutorSearchCriteria tutorSearchCriteria);
}
