package iti.soir.myapplication.tutor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import iti.soir.myapplication.R;

public class TutorAdapter extends RecyclerView.Adapter<TutorViewHolder> {
    private List<Tutor> tutorList = new ArrayList<>();
    private Context context;

    public void updateTutors(List<Tutor> tutorList) {
        this.tutorList = tutorList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return tutorList.size();
    }

    @Override
    public void onBindViewHolder(TutorViewHolder tutorViewHolder, int i) {
        Tutor tutor = tutorList.get(i);
        tutorViewHolder.vTutorName.setText(tutor.getFullName());
        tutorViewHolder.vTutorDescription.setText(tutor.description);

        NumberFormat formatFloat = new DecimalFormat("#0.0");
        tutorViewHolder.vTutorRating.setText(formatFloat.format(tutor.rating));

        int profilePictureId = context.getResources().getIdentifier("picture_" + tutor.id.toLowerCase(), "drawable", context.getPackageName());
        if (profilePictureId != 0) {
            tutorViewHolder.vTutorProfilePicture.setImageResource(profilePictureId);
        } else {
            tutorViewHolder.vTutorProfilePicture.setImageResource(R.drawable.profilepicture1);
        }

        //NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("fr-CH"));
        //tutorViewHolder.vPrice.setText(formatter.format(tutor.price));

        //Date d = new Date(tutor.date);
        //DateFormat f = new SimpleDateFormat("dd MMM - HH:mm");
        //tutorViewHolder.vDate.setText(f.format(d));
    }

    @Override
    public TutorViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        View itemView = LayoutInflater.
                from(this.context).
                inflate(R.layout.activity_tutors_card, viewGroup, false);

        return new TutorViewHolder(itemView);
    }
}
