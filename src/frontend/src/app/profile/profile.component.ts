import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {SelectItem} from 'primeng/api';
import {Tutor, User, UserType} from '../models';
import {Lookup, LookupService} from '../lookup.service';
import {UserService} from "../user.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  qualificationFormulaire: SelectItem[];
  qualification: SelectItem[] = [];
  qualificationSelected: { [subjectId: string]: string[] } = {};
  pricesSelected: { [subjectId: string]: number } = {};
  subjects: Lookup[] = [];  // Store all subjects area
  selectedSubjects: string[] = [];  // Store all subjects area
  user: User; // Store user object in local
  price: number[]; // Store price in local
  form: FormGroup;

  userType = UserType;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private lookupService: LookupService,
    private userService: UserService) {
    // Get Tutor from backend

    const user=userService.loggedInUser;

        this.user = user;
        this.selectedSubjects = [];
        if (user.type === UserType.tutor) {
          (user as Tutor).classes.forEach((cours) => {
            this.selectedSubjects.push(cours.subject);
            this.qualificationSelected[cours.subject] = cours.levels;
            this.pricesSelected[cours.subject] = cours.price;
          });
          console.log(this.user);
        }
    lookupService.getSubjects().subscribe(subjects => this.subjects = subjects);
    lookupService.getLevels().subscribe(levels => this.qualification = levels.map(this.lookupToSelectItem));
  }

  private lookupToSelectItem(lookup: Lookup): SelectItem {
    return {
      label: lookup.name,
      value: lookup.id,
    } as SelectItem;
  }

  ngOnInit() {
  }

  qualificationsChanged() {
    console.log(this.qualificationSelected);
  }

  validateForm(user: User) {
    (user as Tutor).classes.forEach(function (index) {
      if (index.subject !== undefined) {
        if ((index.price !== undefined) && index.subject !== undefined) {
          console.log("correct form");
          console.log(user);
          return true;
        } else {
          console.log("invalid form");
          console.log(user);
          return false;
        }
      }
    })
  }

  onSubmit() {
    // Transform le tableau de string en un objet.
    if (this.user.type === UserType.tutor) {
      (this.user as Tutor).classes = this.selectedSubjects.map((subjectId) => ({
        subject: subjectId,
        levels: this.qualificationSelected[subjectId],
        price: this.pricesSelected[subjectId]
      }));
      console.log(this.user);
    }
    this.userService.putUser(this.user).subscribe(tutor => this.user = tutor);
  }
}
