import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/api';
import { UserService } from '../user.service';
import { SessionService } from '../session.service';
import { ReviewService } from '../review.service';
import { UserType, Session, Review, Note } from '../models';

@Component({
  selector: 'app-lessons-session',
  templateUrl: './lessons.session.component.html',
  styleUrls: ['./lessons.session.component.scss']
})
export class LessonsSessionComponent implements OnInit {

  USER_TYPES: typeof UserType = UserType;

  userType: UserType;
  private session: Session;
  private inProgress: boolean = true;
  private stars: number;
  private comment: string;
  private note: string;

  constructor(
    private userService: UserService,
    private sessionsService: SessionService,
    private reviewService: ReviewService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig
  ) {
  }

  ngOnInit() {
    this.userType = this.userService.loggedInUser.type;
    this.session = this.config.data.session;
    this.inProgress = true;
    this.stars = 0;
    this.comment = "";
    this.note = "";
  }

  endSession() {
    this.inProgress = false;
  }

  skipReview() {
    this.sessionsService.makeSessionDone(this.session).subscribe(x => {
      this.ref.close(null);
    });
  }

  postReview() {
    this.sessionsService.makeSessionDone(this.session).subscribe(x => {
      let newReview: Review = {
        id: null,
        stars: this.stars,
        comment: this.comment,
        tutor: this.session.tutor
      }
      this.reviewService.makeReview(newReview).subscribe(id => {
        newReview.id = id;
        this.ref.close(newReview);
      });
    });
  }

  skipNote() {
    this.sessionsService.makeSessionDone(this.session).subscribe(x => {
      this.ref.close(null);
    });
  }

  postNote() {
    this.sessionsService.makeSessionDone(this.session).subscribe(x => {
      let newNote: Note = {
        id: null,
        note: this.note,
        tutor: this.session.tutor,
        learner: this.session.learner,
      }
      this.ref.close(newNote);
    });
  }

}
