import { Component, OnInit } from '@angular/core';
import { DialogService, MessageService } from 'primeng/api';

import { UserService } from '../user.service';
import { SessionService } from '../session.service';
import { Session, UserType, SessionStatusType, Review, Note } from '../models';
import {LessonsSessionComponent} from './lessons.session.component';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss'],
  providers: [DialogService, MessageService],
})

export class LessonsComponent implements OnInit {

  USER_TYPES: typeof UserType = UserType;
  SESSION_STATUS_TYPES: typeof SessionStatusType = SessionStatusType;
  CURRENT_DATE: number = Date.now();

  userType: UserType;
  sessionRequested: Session[];
  sessionNext: Session;
  sessionFutures: Session[];
  sessionPasts: Session[];

  constructor(
    private userService: UserService,
    private sessionsService: SessionService,
    private messageService: MessageService,
    public dialogService: DialogService
  ) {
    this.userType = this.userService.loggedInUser.type;
    this.sessionRequested = [];
    this.sessionNext = null;
    this.sessionFutures = [];
    this.sessionPasts = [];
  }

  ngOnInit() {
    this.loadSessions();
  }

  loadSessions() {
    this.CURRENT_DATE = Date.now();

    ((this.userService.loggedInUser.type !== UserType.parent) ? this.sessionsService.getSessions() : this.sessionsService.getChildrenSessions())
      .subscribe(sessions => {
        this.sessionRequested.splice(0, this.sessionRequested.length);
        this.sessionNext = null;
        this.sessionFutures.splice(0, this.sessionFutures.length);
        this.sessionPasts.splice(0, this.sessionPasts.length);

        for (const session of sessions) {
          if (session.status === SessionStatusType.requested) {
            this.sessionRequested.push(session);
          } else if ((session.status === SessionStatusType.confirmed) || (session.status === SessionStatusType.ongoing)) {
            if ((session.date + session.duration * 3600000) < Date.now()) {
              this.sessionPasts.push(session);
            } else {
              this.sessionFutures.push(session);
            }
          } else {
            this.sessionPasts.push(session);
          }
        }

        if (this.sessionFutures.length > 0) {
          this.sessionNext = this.sessionFutures.pop();
        }
      });
  }

  confirmSession(session: Session) {
    this.sessionsService.confirmSession(session).subscribe(x => {
      this.messageService.add({severity:'info', summary: 'Session request confirmed', detail: ''});
      this.loadSessions();
    });
  }

  rejectSession(session: Session) {
    this.sessionsService.rejectSession(session).subscribe(x => {
      this.messageService.add({severity:'info', summary: 'Session request rejected', detail: ''});
      this.loadSessions();
    });
  }

  joinSession(session: Session) {
    const ref = this.dialogService.open(LessonsSessionComponent, {
      data: {
        session: session,
      },
      header: 'Session'
    });

    if (this.userService.loggedInUser.type === UserType.learner) {
      ref.onClose.subscribe((review: Review) => {
        if (review) {
          this.messageService.add({severity:'success', summary: 'Thanks for your review !', detail: ''});
        }
        this.loadSessions();
      });
    } else {
      ref.onClose.subscribe((note: Note) => {
        if (note) {
          this.messageService.add({severity:'success', summary: 'Your note has been saved', detail: ''});
        }
        this.loadSessions();
      });
    }
  }

}
