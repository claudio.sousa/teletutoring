import { Component } from '@angular/core';
import { MenuItem, SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginFormComponent } from './login-form/login-form.component';
import { UserService } from './user.service';
import { UserType, User } from './models';
import { Observable } from 'rxjs';
import { withLatestFrom } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss'
  ],
})
export class AppComponent {

  title = 'Teletutoring';

  menuItems: MenuItem[] = [];
  languages: SelectItem[] = [];

  constructor(
    private translateService: TranslateService,
    private userService: UserService,
    private modalService: NgbModal
  ) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translateService.setDefaultLang('en');
    translateService.addLangs(['fr']);

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translateService.use('en').subscribe(() => {
      this.initLanguages();
    });

    translateService.onLangChange.subscribe(() => this.reloadMenu());
    userService.$loggedInUser.subscribe(() => this.reloadMenu());
  }

  initLanguages() {
    this.languages = this.translateService.langs.map(lang => ({
      label: this.translateService.instant(lang),
      value: lang,
    }));
  }

  set language(lang: string) {
    this.translateService.use(lang);
  }

  get isLoggedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  get language() {
    return this.translateService.currentLang;
  }

  logout() {
    this.userService.logout();
    this.reloadMenu();
  }

  async login() {
    await this.modalService.open(LoginFormComponent, { windowClass: 'loginmodal-container' }).result;
  }

  initMenu() {
    this.reloadMenu()
  }

  reloadMenu() {
    this.menuItems = [
      {
        label: this.title,
        style: {
          'font-weight': 'bold',
        },
        routerLink: ['home'],
      }]

    if (!this.userService.loggedInUser)
      return;

    this.menuItems = [...this.menuItems,
    ...(this.userService.loggedInUser.type == UserType.learner
      ? [{
        label: this.translateService.instant('FIND_TUTOR'),
        icon: 'pi pi-fw pi-search',
        routerLink: ['tutors'],
      }]
      : []),
    {
      label: this.translateService.instant('LESSONS'),
      icon: 'pi pi-fw pi-briefcase',
      routerLink: ['lessons'],
    },
    ...(this.userService.loggedInUser.type != UserType.parent
      ? [
        {
          label: this.translateService.instant('AGENDA'),
          icon: 'pi pi-fw pi-calendar',
          routerLink: ['agenda'],
        },
        {
          label: this.translateService.instant('PROFILE'),
          icon: 'pi pi-fw pi-user',
          routerLink: ['profile'],
        }]
      : []),
    ...(this.userService.loggedInUser.type == UserType.parent
      ? [
        {
          label: this.translateService.instant('TRANSACTIONS'),
          icon: 'pi pi-fw pi-money-bill',
          routerLink: ['transactions'],
        },
        {
          label: this.translateService.instant('CHILDREN'),
          icon: 'pi pi-fw pi-users',
          routerLink: ['children']
        }]
      : [])
    ];
  }
}
