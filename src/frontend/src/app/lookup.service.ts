import {Injectable} from '@angular/core';
import {BackendService} from './backend.service';
import {flatMap, map, toArray} from 'rxjs/operators';
import {identity, Observable} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class LookupService {

  constructor(private backendService: BackendService,
              private translateService: TranslateService) {
  }

  getSubjects() {
    return this.getLookup('subjects');
  }

  getLevels() {
    return this.getLookup('levels');
  }

  getLanguages() {
    return this.getLookup('languages');
  }

  private getLookup(lookupName: string): Observable<Lookup[]> {
    return this.backendService.getMany<string>(`lookups/${lookupName}`)
      .pipe(
        flatMap(ids => ids),
        flatMap(id => this.translateService.get(id)
          .pipe(map(name => ({id, name} as Lookup)))),
        toArray(),
      );
  }
}

export interface Lookup {
  id: string;
  name: string;
}
