import {Component, OnInit} from '@angular/core';
import {ConfirmationService, DialogService, MessageService, SelectItem} from 'primeng/api';
import {TutorFilter, TutorService} from './tutor.service';
import {SessionService} from '../session.service';
import {UserService} from '../user.service';
import {Session, Tutor, TutorClass} from '../models';
import {TutorReserveComponent} from './tutor.reserve.component';
import * as _ from 'lodash';
import {Lookup, LookupService} from '../lookup.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-tutors',
  templateUrl: './tutors.component.html',
  styleUrls: ['./tutors.component.scss'],
  providers: [ConfirmationService, DialogService, MessageService],
})
export class TutorsComponent implements OnInit {

  private static emptySelectItem: SelectItem = {
    label: '',
    value: null,
  };

  minPrice = 0;
  maxPrice = 100;

  tutorFilter: TutorFilter = {
    filter: {
      price_range: [
        this.minPrice,
        this.maxPrice,
      ],
      availability: {},
    },
  };

  tutors: TutorSearchResult[] = [];

  subjects: SelectItem[] = [TutorsComponent.emptySelectItem];
  levels: SelectItem[] = [TutorsComponent.emptySelectItem];

  availabilityImmediate = true;
  availabilityMorning = false;
  availabilityAfternoon = false;
  availabilityEvening = false;

  availabilityMonday = false;
  availabilityTuesday = false;
  availabilityWednesday = false;
  availabilityThursday = false;
  availabilityFriday = false;
  availabilitySaturday = false;
  availabilitySunday = false;

  availabilityTextItems: string[] = [];

  noTutorFound = false;

  constructor(
    private userService: UserService,
    private tutorService: TutorService,
    private sessionsService: SessionService,
    private messageService: MessageService,
    private lookupService: LookupService,
    private confirmationService: ConfirmationService,
    private translateService: TranslateService,
    public dialogService: DialogService,
  ) {
  }

  ngOnInit() {
    this.loadTutors();
    this.loadSubjects();
    this.loadLevels();
    this.refreshAvailabilityFilter(true);

    this.translateService.onLangChange.subscribe(() => {
      this.loadSubjects();
      this.loadLevels();
      this.refreshAvailabilityFilter(true);
    });
  }

  loadTutors() {
    if (this.tutorFilter.filter.level && this.tutorFilter.filter.subject) {
      this.tutorService.searchTutors(this.cleanTutorFilter)
        .subscribe(tutors => {
          this.tutors = tutors.map(this.createTutorSearchResult.bind(this));
          this.noTutorFound = this.tutors.length === 0;
        });
    }
  }

  loadSubjects() {
    this.lookupService.getSubjects().subscribe(subjects =>
      this.subjects = [TutorsComponent.emptySelectItem, ...subjects.map(this.lookupToSelectItem)]);
  }

  loadLevels() {
    this.lookupService.getLevels().subscribe(levels =>
      this.levels = [TutorsComponent.emptySelectItem, ...levels.map(this.lookupToSelectItem)]);
  }

  refreshAvailabilityFilter(immediateChanged = false) {
    const newTextItems: string[] = [];

    if (immediateChanged && this.availabilityImmediate) {
      this.availabilityMonday = false;
      this.availabilityTuesday = false;
      this.availabilityWednesday = false;
      this.availabilityThursday = false;
      this.availabilityFriday = false;
      this.availabilitySaturday = false;
      this.availabilitySunday = false;
      this.availabilityMorning = false;
      this.availabilityAfternoon = false;
      this.availabilityEvening = false;
      newTextItems.push('Immediate');
    } else {
      this.availabilityImmediate = false;
    }

    this.tutorFilter.availableNow = this.availabilityImmediate;

    const hourRanges: number[][] = [];
    if (this.availabilityMorning) {
      hourRanges.push([6, 12]);
      newTextItems.push('Morning');
    }
    if (this.availabilityAfternoon) {
      hourRanges.push([12, 17]);
      newTextItems.push('Afternoon');
    }
    if (this.availabilityEvening) {
      hourRanges.push([17, 23]);
      newTextItems.push('Evening');
    }

    this.tutorFilter.filter.availability.hour_ranges =
      hourRanges.length > 0 ? hourRanges : null;

    const daysOfWeek = [];
    if (this.availabilityMonday) {
      daysOfWeek.push(1);
      newTextItems.push('Monday');
    }
    if (this.availabilityTuesday) {
      daysOfWeek.push(2);
      newTextItems.push('Tuesday');
    }
    if (this.availabilityWednesday) {
      daysOfWeek.push(3);
      newTextItems.push('Wednesday');
    }
    if (this.availabilityThursday) {
      daysOfWeek.push(4);
      newTextItems.push('Thursday');
    }
    if (this.availabilityFriday) {
      daysOfWeek.push(5);
      newTextItems.push('Friday');
    }
    if (this.availabilitySaturday) {
      daysOfWeek.push(6);
      newTextItems.push('Saturday');
    }
    if (this.availabilitySunday) {
      daysOfWeek.push(0);
      newTextItems.push('Sunday');
    }

    this.tutorFilter.filter.availability.daysOfWeek = daysOfWeek.length === 0
      ? null
      : daysOfWeek;

    this.availabilityTextItems = newTextItems.map(item => this.translateService.instant(item));

    this.loadTutors();
  }

  reserve(selectedTutor: Tutor) {
    const ref = this.dialogService.open(TutorReserveComponent, {
      data: {
        tutor: selectedTutor,
        now: false,
        researchedSubject: this.tutorFilter.filter.subject,
        researchedLevel: this.tutorFilter.filter.level
      },
      header: 'Reserve a session with ' + selectedTutor.firstName + ' ' + selectedTutor.lastName
    });

    ref.onClose.subscribe((session: Session) => {
      if (session) {
        this.messageService.add({severity: 'info', summary: 'Session request send', detail: ''});
      }
    });
  }

  requestImmediateSession(selectedTutor: Tutor) {
    const ref = this.dialogService.open(TutorReserveComponent, {
      data: {
        tutor: selectedTutor,
        now: true,
        researchedSubject: this.tutorFilter.filter.subject,
        researchedLevel: this.tutorFilter.filter.level
      },
      header: 'Reserve a session with ' + selectedTutor.firstName + ' ' + selectedTutor.lastName
    });

    ref.onClose.subscribe((session: Session) => {
      if (session) {
        this.messageService.add({severity: 'info', summary: 'Session request send', detail: ''});
      }
    });
  }

  private createTutorSearchResult(tutor: Tutor): TutorSearchResult {
    return {
      ...tutor,
      classes: tutor.classes.sort(tutorClass => +!this.tutorClassMatchesTutorFilter(tutorClass)),
      stars: _.range(5).map(i => i < tutor.rating),
    };
  }

  private lookupToSelectItem(lookup: Lookup): SelectItem {
    return {
      label: lookup.name,
      value: lookup.id,
    } as SelectItem;
  }

  private get cleanTutorFilter() {
    function isEmptyRecursive(obj: any, maxNesting: number) {
      if (_.isEmpty(obj)) {
        return true;
      }

      if (maxNesting <= 0) {
        return false;
      }

      return _.every(obj, subObj => isEmptyRecursive(subObj, maxNesting - 1));
    }

    return _.omitBy(this.tutorFilter, x => isEmptyRecursive(x, 2));
  }

  private tutorClassMatchesTutorFilter(tutorClass: TutorClass) {
    return tutorClass.subject && tutorClass.levels && tutorClass.price &&
           this.tutorFilter.filter.subject === tutorClass.subject &&
           _.some(tutorClass.levels, level => level === this.tutorFilter.filter.level) &&
           this.tutorFilter.filter.price_range[0] <= tutorClass.price &&
           this.tutorFilter.filter.price_range[1] >= tutorClass.price;
  }
}

type TutorSearchResult = Tutor & {
  stars: boolean[];
};
