import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/api';
import {TutorService} from './tutor.service';
import { SessionService } from '../session.service';
import { UserService } from '../user.service';
import {Tutor, TutorClass, Session, SessionStatusType, UserType} from '../models';

@Component({
  selector: 'app-tutor-reserve',
  templateUrl: './tutor.reserve.component.html',
  styleUrls: ['./tutor.reserve.component.scss']
})
export class TutorReserveComponent implements OnInit {

  private tutor: Tutor;
  private now: boolean;
  hoursToReserve: number;
  classes: any[];
  selectedclass: TutorClass;
  selectedDateTime: Date;

  constructor(
    private userService: UserService,
    private tutorService: TutorService,
    private sessionsService: SessionService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig
  ) {
  }

  ngOnInit() {
    this.tutor = this.config.data.tutor;
    this.now = this.config.data.now;
    this.hoursToReserve = 1;
    this.classes = this.tutor.classes.map(tutorClass => ({ 'label': `${tutorClass.subject} ${tutorClass.levels} (${tutorClass.price} CHF/Hours)`, value: tutorClass }));
    this.selectedDateTime = new Date();
    this.selectedDateTime.setMinutes(new Date().getMinutes() + (30 - (new Date().getMinutes() % 30)));
    this.selectedDateTime.setSeconds(0);

    // Automatcally select the researched classes
    let foundTutorClass = this.tutor.classes.find(tutorClass =>
      (tutorClass.subject == this.config.data.researchedSubject) &&
        (tutorClass.levels.find(level => level == this.config.data.researchedLevel ) == this.config.data.researchedLevel)
    );
    if (foundTutorClass) {
      this.selectedclass = foundTutorClass;
    }
  }

  sendRequest() {
    let newSession: Session = {
      id: null,
      date: this.selectedDateTime.getTime(),
      tutoredClass: this.selectedclass,
      duration: this.hoursToReserve,
      tutor: this.tutor,
      learner: this.userService.loggedInUser,
      status: SessionStatusType.requested
    }
    this.sessionsService.requestNewSession(newSession).subscribe(id => {
        newSession.id = id;
        this.ref.close(newSession);
      }
    );
  }

}
