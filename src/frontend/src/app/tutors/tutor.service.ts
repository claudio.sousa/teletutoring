import {Injectable} from '@angular/core';
import {BackendService} from '../backend.service';
import {Observable} from 'rxjs';
import {Availability, Tutor} from '../models';

@Injectable({
  providedIn: 'root',
})
export class TutorService {

  constructor(
    private backendService: BackendService,
  ) {
  }

  getTutors(): Observable<Tutor[]> {
    return this.backendService.getMany<Tutor>('tutors');
  }

  getTutor(id: string): Observable<Tutor> {
    return this.backendService.getOne<Tutor>(`tutors/${id}`);
  }

  putTutor(tutor: Tutor): Observable<Tutor> {
    return this.backendService.putOne<Tutor, Tutor>(`tutors/${tutor.id}`, tutor);
  }

  searchTutors(filter: TutorFilter) {
    return this.backendService.postMany('tutors/search', filter);
  }

  getAvailabilities(tutorId: string) {
    return this.backendService.getMany<Availability>(`tutors/${tutorId}/availability`);
  }

  postAvailability(tutorId: string, availability: Availability) {
    return this.backendService.postOne(`tutors/${tutorId}/availability`, availability);
  }

  putAvailability(tutorId: string, availabilityId: string, availability: Availability) {
    return this.backendService.putOne(`tutors/${tutorId}/availability/${availabilityId}`, availability);
  }

  deleteAvailability(tutorId: string, availabilityId: string) {
    return this.backendService.delete(`tutors/${tutorId}/availability/${availabilityId}`);
  }
}

export interface TutorFilter {
  availableNow?: boolean;
  filter?: {
    price_range?: number[];
    subject?: string;
    level?: string;
    availability?: {
      daysOfWeek?: number[];
      hour_ranges?: number[][];
    };
  };
}
