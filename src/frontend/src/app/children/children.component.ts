import { Component, OnInit } from '@angular/core';
import { ChildrenService } from './children.service';
import {User} from '../models';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss'],
  providers: [ConfirmationService]
})
export class ChildrenComponent implements OnInit {

  public children:User[];

  constructor(private childrenService: ChildrenService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.loadChildren();
  }

  async loadChildren(){
   this.children = await this.childrenService.getChildren().toPromise()
  }

  addChild() {

  }

  deleteChild(child: User) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this child (' + child.firstName + ' ' + child.lastName + ') ?',
      accept: () => {
          //Actual logic to perform a confirmation
      }
    });
  }
}
