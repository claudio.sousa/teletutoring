import { Injectable } from '@angular/core';
import {BackendService} from '../backend.service';
import {User} from '../models';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChildrenService {

  constructor( private backendService: BackendService) { }

  getChildren(): Observable<User[]> {
    return this.backendService.getMany<User | null>('users/children')
  }

}
