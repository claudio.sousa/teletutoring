import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {Tutor, User, UserType} from './models';
import { BackendService } from './backend.service';
import { Observable, Subject } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public $loggedInUser: Subject<User | null>;
  public loggedInUser: User | null;
  public loadingUserInfo: Promise<boolean>;

  constructor(
    private backendService: BackendService,
    private router: Router
  ) {
    this.$loggedInUser = new Subject<User | null>();
    this.$loggedInUser.subscribe(
      user => this.loggedInUser = user,
      () => this.logout()
    );
    this.loadingUserInfo = this.loadUserInformation();
  }

  public async loadUserInformation() {
    let user;
    try {
      user = await this.getUser().toPromise();
    }
    catch {
    }
    this.$loggedInUser.next(user);
    return !!user;
  }

  private getUser(): Observable<User | null> {
    return this.backendService.getOne<User | null>('users/me')
  }

  isLoggedIn(): boolean {
    return !!this.loggedInUser;
  }

  userType(): UserType | null {
    if (!this.loggedInUser)
      return null;
    return this.loggedInUser.type;
  }

  async logout() {
    this.loggedInUser = null;
    this.router.navigate(['']);
    this.$loggedInUser.next(null);
    return this.backendService.postOne<any, any>(
      `authentication/logout`, {}
    ).toPromise();
  }

  async login(username: string, password: string): Promise<boolean> {
    type Credentials = { username: string, password: string };
    const loggedUser = await this.backendService.postOne<User | null, Credentials>(
      `authentication/login`,
      { username, password }
    ).toPromise();

    this.$loggedInUser.next(loggedUser);
    return !!loggedUser;
  }

  putUser(user: User): Observable<User> {
    return this.backendService.putOne<User, User>(`users/${user.id}`, user);
  }
}
