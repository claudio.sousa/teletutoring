import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../user.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Input() username: string;
  @Input() password: string;

  constructor(
    public activeModal: NgbActiveModal,
    private userService: UserService) { }

  ngOnInit() {
  }

  async onSubmit() {
    const success = await this.userService.login(this.username, this.password);
    if (!success) {
      alert('Wrong username or password!');
      this.password = '';
    } else {
      this.activeModal.close();
    }
  }
}
