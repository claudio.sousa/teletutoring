import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { SliderModule } from 'primeng/slider';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { FieldsetModule } from 'primeng/fieldset';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { OverlayPanelModule, ToggleButtonModule, TooltipModule } from 'primeng/primeng';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ToastModule } from 'primeng/toast';
import { RatingModule } from 'primeng/rating';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { InputTextareaModule } from 'primeng/inputtextarea';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LessonsComponent } from './lessons/lessons.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ProfileComponent } from './profile/profile.component';
import { TutorsComponent } from './tutors/tutors.component';
import { TutorReserveComponent } from './tutors/tutor.reserve.component';
import { LessonsSessionComponent } from './lessons/lessons.session.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AvailabilityComponent } from './agenda/availability.component';
import { HomeComponent } from './home/home.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ChildrenComponent } from './children/children.component';
import {FullCalendarModule} from 'ng-fullcalendar';
import { CreditAccountComponent } from './transactions/credit-account/credit-account.component';
import { TransactionsComponent } from './transactions/transactions.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LessonsComponent,
    AgendaComponent,
    ProfileComponent,
    TutorsComponent,
    TutorReserveComponent,
    LessonsSessionComponent,
    AvailabilityComponent,
    HomeComponent,
    LoginFormComponent,
    ChildrenComponent,
    CreditAccountComponent,
    TransactionsComponent,
  ],
  entryComponents: [
    TutorReserveComponent,
    LessonsSessionComponent,
    AvailabilityComponent,
    LoginFormComponent,
    CreditAccountComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    NgbModule,
    MenubarModule,
    ButtonModule,
    PanelModule,
    SliderModule,
    DropdownModule,
    MultiSelectModule,
    FieldsetModule,
    TableModule,
    CalendarModule,
    OverlayPanelModule,
    ConfirmDialogModule,
    DynamicDialogModule,
    ToastModule,
    RatingModule,
    ChipsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextareaModule,
    CheckboxModule,
    ToggleButtonModule,
    TooltipModule,
    FullCalendarModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
