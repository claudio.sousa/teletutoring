import {Component, OnInit} from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/api';
import {Availability} from '../models';
import {TutorService} from '../tutors/tutor.service';
import {UserService} from '../user.service';

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.scss'],
})
export class AvailabilityComponent implements OnInit {
  private reloadEvents: () => void;
  startTime: Time;
  endTime: Time;

  startRecur: Date;
  endRecur: Date;

  availability: Availability;
  availabilityMonday = false;
  availabilityTuesday = false;
  availabilityWednesday = false;
  availabilityThursday = false;
  availabilityFriday = false;
  availabilitySaturday = false;
  availabilitySunday = false;

  private static timestampToTime(timestamp: number): Time {
    const date = new Date(timestamp);
    return {
      hour: date.getHours(),
      minute: date.getMinutes(),
    };
  }

  constructor(
    private tutorService: TutorService,
    private userService: UserService,
    private dynamicDialogRef: DynamicDialogRef,
    private dynamicDialogConfig: DynamicDialogConfig,
  ) {
  }

  ngOnInit() {
    const date = this.dynamicDialogConfig.data.date;
    const timestamp = date.getTime();

    this.startRecur = new Date(timestamp);
    this.startRecur.setHours(0);
    this.startRecur.setMinutes(0);
    this.startRecur.setMilliseconds(0);

    this.endRecur = new Date(timestamp);
    this.endRecur.setHours(23);
    this.endRecur.setMinutes(59);
    this.endRecur.setMilliseconds(0);

    this.availability = {
      id: null,
      startRecur: timestamp,
      endRecur: null,
      daysOfWeek: [],
      startTime: timestamp,
      endTime: timestamp + 2 * 60 * 60 * 1000,
    };

    this.startTime = AvailabilityComponent.timestampToTime(this.availability.startTime);
    this.endTime = AvailabilityComponent.timestampToTime(this.availability.endTime);

    this.reloadEvents = this.dynamicDialogConfig.data.reloadEvents;
  }

  saveAvailability() {
    this.availability.daysOfWeek = [];
    if (this.availabilityMonday) {
      this.availability.daysOfWeek.push(1);
    }
    if (this.availabilityTuesday) {
      this.availability.daysOfWeek.push(2);
    }
    if (this.availabilityWednesday) {
      this.availability.daysOfWeek.push(3);
    }
    if (this.availabilityThursday) {
      this.availability.daysOfWeek.push(4);
    }
    if (this.availabilityFriday) {
      this.availability.daysOfWeek.push(5);
    }
    if (this.availabilitySaturday) {
      this.availability.daysOfWeek.push(6);
    }
    if (this.availabilitySunday) {
      this.availability.daysOfWeek.push(0);
    }

    this.availability.startRecur = this.startRecur.getTime();
    this.availability.endRecur = this.endRecur.getTime();

    const date = new Date(this.availability.startRecur);

    date.setHours(this.startTime.hour);
    date.setMinutes(this.startTime.minute);
    this.availability.startTime = date.getTime();

    date.setHours(this.endTime.hour);
    date.setMinutes(this.endTime.minute);
    this.availability.endTime = date.getTime();

    this.tutorService.postAvailability(this.userService.loggedInUser.id, this.availability)
      .subscribe(() => {
        this.reloadEvents();
        this.dynamicDialogRef.close();
      });
  }
}

interface Time {
  hour: number;
  minute: number;
}
