import {Component, OnInit, ViewChild} from '@angular/core';
import {SessionService} from '../session.service';
import {Session, UserType} from '../models';
import {ConfirmationService, DialogService, OverlayPanel} from 'primeng/primeng';
import {TranslateService} from '@ngx-translate/core';
import {AvailabilityComponent} from './availability.component';
import {UserService} from '../user.service';
import {TutorService} from '../tutors/tutor.service';
import {flatMap, map, toArray} from 'rxjs/operators';
import {identity, merge, Observable} from 'rxjs';
import {EventInput} from '@fullcalendar/core';
import {OptionsInput} from '@fullcalendar/core/types/input-types';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss'],
  providers: [DialogService],
})
export class AgendaComponent implements OnInit {

  @ViewChild('sessionPanel') overlayPanel: OverlayPanel;

  selectedSession: Session;

  calendarEvents: EventInput[];

  calendarOptions: OptionsInput = {
    editable: true,
    selectable: true,
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    plugins: [
      dayGridPlugin, timeGridPlugin, interactionPlugin
    ],
    eventClick: ({el, event}) => {
      console.log(event);
      if (event.extendedProps.isAvailability) {
        this.tutorService.deleteAvailability(this.userService.loggedInUser.id, event.id)
            .subscribe(() => this.loadSessions())
      }
      else {
        this.selectedSession = event.extendedProps;
        this.overlayPanel.show(event, el);
      }

      console.log(this.selectedSession);
    },
    dateClick: ({date}) => {
      if (this.userService.loggedInUser.type === UserType.tutor) {
        this.dialogService.open(AvailabilityComponent, {
          header: this.translateService.instant('Availability'),
          data: {
            date,
            reloadEvents: this.loadSessions.bind(this),
          },
        });
      }
    },
  };

  constructor(
    private sessionsService: SessionService,
    private translateService: TranslateService,
    private userService: UserService,
    private tutorService: TutorService,
    private dialogService: DialogService,
  ) {
  }

  get language() {
    return this.translateService.currentLang;
  }

  ngOnInit() {
    this.loadSessions();
  }

  loadSessions() {
    const sessionsEventsObservable =
      this.sessionsService.getSessions()
        .pipe(
          flatMap(identity),
          map((session: Session) => ({
            title: (this.userService.loggedInUser.type == UserType.learner) ?
                `${session.tutor.firstName} ${session.tutor.lastName} ${session.duration}h` :
                `${session.learner.firstName} ${session.learner.lastName} ${session.duration}h`,
            start: session.date,
            end: session.date + session.duration * 60 * 60 * 1000,
            extendedProps: {
              isAvailability:false,
              ...session
            },
          } as EventInput)),
        );

    const eventsObservables: Observable<EventInput>[] = [sessionsEventsObservable];

    const loggedInUser = this.userService.loggedInUser;

    if (loggedInUser.type === UserType.tutor) {
      const availabilitiesObservable =
        this.tutorService.getAvailabilities(loggedInUser.id)
          .pipe(
            flatMap(identity),
            map(availability => {
              const startTime = new Date(availability.startTime).toTimeString().substr(0, 5);
              const endTime = new Date(availability.endTime).toTimeString().substr(0, 5);
              const event = {
                title: `${startTime} - ${endTime}`,
                color: 'green',
                ...availability,
                startTime,
                endTime,
                extendedProps: {
                  isAvailability: true,
                  ...availability
                }
              };

              if (event.daysOfWeek.length === 0) {
                delete event.daysOfWeek;
              }

              return event;
            }),
          );

      eventsObservables.push(availabilitiesObservable);
    }

    merge(eventsObservables)
      .pipe(
        flatMap(identity),
        toArray(),
      )
      .subscribe(events => {
        console.log(events);
        return this.calendarEvents = events;
      });
  }
}
