import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TutorsComponent } from './tutors/tutors.component';
import { LessonsComponent } from './lessons/lessons.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ProfileComponent } from './profile/profile.component';
import { ChildrenComponent } from './children/children.component';
import { AuthGuardService } from './auth-guard.service';
import { TransactionsComponent } from './transactions/transactions.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'tutors',
    component: TutorsComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'lessons',
    component: LessonsComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'agenda',
    component: AgendaComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'transactions',
    component: TransactionsComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'children',
    component: ChildrenComponent,
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
