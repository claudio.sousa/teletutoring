import {Injectable} from '@angular/core';
import {Review, IReviewDB} from './models';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private baseUrl = 'reviews/';

  constructor(private backendService: BackendService) {
  }

  constructFromReview(review: Review): IReviewDB {
    return {
      id: review.id,
      rating: review.stars,
      comment: review.comment
    };
  }

  makeReview(newReview: Review): Observable<string> {
    let reviewInsert: IReviewDB = this.constructFromReview(newReview);
    return this.backendService.postOne<string, IReviewDB>(this.baseUrl + newReview.tutor.id, reviewInsert);
  }
}
