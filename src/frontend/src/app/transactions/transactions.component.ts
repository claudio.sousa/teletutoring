import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { CreditAccountComponent } from './credit-account/credit-account.component';
import { UserService } from '../user.service';
import { User, ITransaction } from '../models';
import { TransactionService } from './transaction.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
  providers: [DialogService],
})
export class TransactionsComponent implements OnInit {

  user: Observable<User>;
  transactions: ITransaction[];

  constructor(
    private transactionService: TransactionService,
    public userService: UserService,
    private dialogService: DialogService,
    private translateService: TranslateService,
  ) { }

  async ngOnInit() {
    this.loadTransactions();
  }

  loadTransactions() {
    this.transactionService
      .transactions()
      .subscribe(transactions => {
        this.transactions = transactions;
        this.transactions.sort((tr1, tr2) => tr2.timestamp - tr1.timestamp)
      })
  }

  creditAccount() {
    const dialog = this.dialogService.open(CreditAccountComponent, {
      data: {
        user: this.userService.loggedInUser,
      },
      header: this.translateService.instant('CREDIT_ACCOUNT')
    });

    dialog.onClose.subscribe(async (credit: number) => {
      if (credit) {
        await this.transactionService.creditAccount(credit).toPromise();
        await this.userService.loadUserInformation();
        this.loadTransactions();
      }
    });
  }
}
