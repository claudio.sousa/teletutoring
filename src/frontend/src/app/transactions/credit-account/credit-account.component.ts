import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';

@Component({
  selector: 'app-credit-account',
  templateUrl: './credit-account.component.html',
  styleUrls: ['./credit-account.component.scss']
})
export class CreditAccountComponent implements OnInit {

  private amountToCredit: number = 0;

  constructor(
    public ref: DynamicDialogRef
  ) {
  }

  ngOnInit() {
  }

  cancel() {
    this.ref.close();
  }

  creditAccount(amountToCredit: number) {
    this.ref.close(amountToCredit);
  }

}
