import { Injectable } from '@angular/core';
import { UserService } from '../user.service';
import { BackendService } from '../backend.service';
import { ITransaction } from '../models';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(
    private backendService: BackendService,
    private userService: UserService
  ) {

  }

  creditAccount(amount) {
    return this.backendService.postOne<Number | null, { amount: number }>('transactions/credit', { amount })
  }

  transactions() {
    return this.backendService.getMany<ITransaction>('transactions/')
  }
}
