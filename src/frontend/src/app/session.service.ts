import {Injectable} from '@angular/core';
import {Session, SessionStatusType, ISessionDB} from './models';
import {BackendService} from './backend.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private baseUrl = 'sessions/';

  constructor(private backendService: BackendService) {
  }

  constructFromSession(session: Session): ISessionDB {
    return {
      id: session.id,
      date: session.date,
      tutoredClass: session.tutoredClass,
      duration: session.duration,
      tutorId: session.tutor.id,
      learnerId: session.learner.id,
      status: session.status
    };
  }

  getSessions(): Observable<Session[]> {
    return this.backendService.getMany<Session>(this.baseUrl);
  }

  getChildrenSessions(): Observable<Session[]> {
    return this.backendService.getMany<Session>(this.baseUrl + 'children');
  }

  requestNewSession(newSession: Session): Observable<string> {
    let sessionInsert: ISessionDB = this.constructFromSession(newSession);
    sessionInsert.status = SessionStatusType.requested;
    return this.backendService.postOne<string, ISessionDB>(this.baseUrl, sessionInsert);
  }

  confirmSession(session: Session): Observable<ISessionDB> {
    let sessionUpdate: ISessionDB = this.constructFromSession(session);
    sessionUpdate.status = SessionStatusType.confirmed;
    return this.backendService.putOne<ISessionDB, ISessionDB>(this.baseUrl + session.id, sessionUpdate);
  }

  rejectSession(session: Session): Observable<ISessionDB> {
    let sessionUpdate: ISessionDB = this.constructFromSession(session);
    sessionUpdate.status = SessionStatusType.rejected;
    return this.backendService.putOne<ISessionDB, ISessionDB>(this.baseUrl + session.id, sessionUpdate);
  }

  makeSessionDone(session: Session): Observable<ISessionDB> {
    let sessionUpdate: ISessionDB = this.constructFromSession(session);
    sessionUpdate.status = SessionStatusType.done;
    return this.backendService.putOne<ISessionDB, ISessionDB>(this.baseUrl + session.id, sessionUpdate);
  }
}
