import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataResult} from './models';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BackendService {

  private rootUrl = 'api/';

  constructor(private httpClient: HttpClient) {
  }

  get<T>(resource: string): Observable<T | T[]> {
    return this.httpClient.get<DataResult<T>>(this.rootUrl + resource)
      .pipe(map(dataResult => dataResult.data));
  }

  getOne<T>(resource: string): Observable<T> {
    return this.get(resource).pipe(map(one => one as T));
  }

  getMany<T>(resource: string): Observable<T[]> {
    return this.get<T>(resource).pipe(map(many => many as T[]));
  }

  post<T, D>(resource: string, data: D): Observable<T | T[]> {
    return this.httpClient.post<DataResult<T>>(this.rootUrl + resource, data)
      .pipe(map(dataResult => dataResult.data));
  }

  postOne<T, D>(resource: string, data: D): Observable<T> {
    return this.post<T, D>(resource, data).pipe(map(one => one as T));
  }

  postMany<T, D>(resource: string, data: D): Observable<T[]> {
    return this.post<T, D>(resource, data).pipe(map(many => many as T[]));
  }

  put<T, D>(resource: string, data: D): Observable<T | T[]> {
    return this.httpClient.put<DataResult<T>>(this.rootUrl + resource, data)
      .pipe(map(dataResult => dataResult.data));
  }

  putOne<T, D>(resource: string, data: D): Observable<T> {
    return this.put<T, D>(resource, data).pipe(map(one => one as T));
  }

  putMany<T, D>(resource: string, data: D): Observable<T[]> {
    return this.put<T, D>(resource, data).pipe(map(many => many as T[]));
  }

  delete(resource: string): Observable<any> {
    return this.httpClient.delete(resource);
  }
}
