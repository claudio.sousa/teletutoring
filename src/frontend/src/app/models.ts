export enum UserType {
  tutor = 0,
  learner = 1,
  parent = 2
}
export enum SessionStatusType {
  requested = 0,
  confirmed = 1,
  ongoing = 2,
  done = 3,
  canceled = 4,
  rejected = 5
}

export interface DataResult<T> {
  data: T | T[];
}

export interface TutorClass {
  subject: string;
  levels: string[];
  price: number;
}

export interface User {
  id: string;
  firstName: string;
  lastName: string;
  parentId: string | null,
  email: string;
  lastConnection: number;
  languages: string[];
  type: UserType;
}

export interface Tutor extends User {
  description: string;
  verified: boolean;
  availableNow: boolean;
  rating: number;
  classes: TutorClass[];
}

export interface Learner extends User {
}

export interface Session {
  id: string;
  date: number;
  tutoredClass: TutorClass;
  duration: number;
  tutor: User;
  learner: User;
  status: SessionStatusType;
}

export interface ISessionDB {
  id: string;
  date: number;
  tutoredClass: TutorClass;
  duration: number;
  tutorId: string;
  learnerId: string;
  status: SessionStatusType;
}

export interface Review {
  id: string;
  stars: number;
  comment: string;
  tutor: User;
}

export interface Note {
  id: string;
  note: string;
  tutor: User;
  learner: User;
}

export interface IReviewDB {
  id: string;
  rating: number;
  comment: string;
}

export interface Availability {
  id: string;
  daysOfWeek: number[];
  startTime: number;
  endTime: number;
  startRecur: number;
  endRecur: number;
}


export interface ITransaction {
  userId: string;
  amount: number;
  timestamp: number;
}
