

# Profil tutor

GET tutors/{id}

## Example

curl http://localhost:8080/tutors/KS8XOlOrI

## params

id: string

## return

### success

```
{
    firstName: string,
    lastName: string,
    email: string,
    lastConnexion: string (timestamp UTC),
    photo: Blob/Image/Base64,
    languages: [ strings ],
    description: string,
    verified: boolean,
    availableNow: boolean,
    rating: int,
    classes: [
        {
            subject-name: string,
            level: string,
            price: Decimal
        }
    ],
    availabilities: [ availability (see Get availability) ]
}
```

### failure

Code: 200
Message: Tutor not found



# Recherche tuteur

POST tutors/search

##POST ADTA FORMAT:
ISUER & {
  filter:{
    "price_range": [15, 30],
    "subject": "MATH",
    "level": "3P",
    "availability": {
      "daysOfWeek": [0, 2, 6],
      "hour_range": [0, 24]
    }
  }
}

## Example1:
curl -X POST \
  http://localhost:8080/tutors/search \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 473de33e-aad0-522f-f9b0-dfaf60e46137' \
  -d '{
	"filter":{
		"availability":{
			"daysOfWeek":[5, 6],
			"hour_range":[18, 24]
		},
		"subject": "MATH",
		"level":"2P",
		"price_range":[14, 17]
	}
}'

## Example2

Search only verified:
```bash
curl -X POST \
  http://localhost:8080/tutors/search \
  -H 'content-type: application/json' \
  -d '{"verified":true}'
```

## params

```
{
    subject-ids: [ strings ],
    price: Number,
    level-ids: [ strings ],
    availability: (see Get availability)
}


curl http://localhost:8080/tutors

## return

### success

```
{
    tutors: [ tutor (see profil) ]
}
```

# Update tutor

PUT tutors/:tutorId

## Example:

curl -X PUT \
  http://localhost:8080/tutors/KS8XOlOrI \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: b02c6065-ddf0-8e5d-02d8-38d82420381a' \
  -d '{
      "id": "KS8XOlOrI",
      "firstName": "Bruno",
      "lastName": "Da Silva",
      "email": "Bruno@hotmail.com",
      "lastConnexion": 1550694778954,
      "languages": [
        "FR"
      ],
      "description": "barbu",
      "verified": true,
      "availableNow": false,
      "rating": 3.4,
      "type": 0,
      "classes": [
        {
          "subjectName": "Mathematiques",
          "level": 3,
          "price": 15
        }
      ],
      "availabilities": [
        {
          "daysOfWeek": [
            0,
            6
          ],
          "startTime": 1551085200,
          "endTime": 1551114000,
          "startRecur": 1551085200,
          "endRecur": 1553533200,
          "id": "pEvYDFbzR"
        },
         {
          "daysOfWeek": [
            0,
            6
          ],
          "startTime": 1551085200,
          "endTime": 1551114000,
          "startRecur": 1551085200,
          "endRecur": 1553533200,
          "id": "pEvYDFbzR2"
        }
      ]
    }'

## params

id: string

## Data

```
{
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    lastConnexion: string (timestamp UTC),
    photo: Blob/Image/Base64,
    languages: [ strings ],
    description: string,
    verified: boolean,
    availableNow: boolean,
    rating: int,
    classes: [
        {
            subject-name: string,
            level: string,
            price: Decimal
        }
    ],
    availabilities: [ availability (see Get availability) ]
}
```

# Delete tutor

DELETE tutors/:tutorId

## Example:

curl -X DELETE  http://localhost:8080/tutors/KS8XOlOrI