
# Get availability

GET tutor/{tutorId}/availability

## Example:
curl http://localhost:8080/tutors/KS8XOlOrI/availability

## params

tutorId: string

## return

### success

```
{
    data:[
          {
            id: string,
            daysOfWeek: number[],
            startTime: number,
            endTime: number,
            startRecur: number, // date
            endRecur: number,
        }
    ]
}
```

### failure

Message: Tutor not found



# Insert availability

POST tutor/{tutorId}/availability

Data:
 {
        daysOfWeek: number[],
        startTime: number,
        endTime: number,
        startRecur: number, // date
        endRecur: number,
    }

## return
{ data : availabilityId}

## Example:

curl -X POST \
  http://localhost:8080/tutors/KS8XOlOrI/availability \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 8a1113b9-2589-c775-5e60-649523b9de29' \
  -d '{
        "daysOfWeek": [0, 6],
        "startTime": 1551085200,
        "endTime": 1551114000,
        "startRecur": 1551085200,
        "endRecur": 1553533200
    }'


# Delete availability

DELETE tutor/:tutorId/availability/:availabilityId

## Example

curl -X DELETE \
  http://localhost:8080/tutors/KS8XOlOrI/availability/5AyP0rmKl



# Update availability

PUT tutor/:tutorId/availability/:availabilityId

## return

## Example:

curl -X PUT \
  http://localhost:8080/tutors/KS8XOlOrI/availability/pEvYDFbzR \
  -H 'content-type: application/json' \
  -d ' {
          "daysOfWeek": [
            0,
            6
          ],
          "startTime": 1551585200,
          "endTime": 1551184000,
          "startRecur": 1551085200,
          "id": "pEvYDFbzR"
    }'

