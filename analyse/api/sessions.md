# Get self sessions (futur/present/past)

GET sessions

## params

token: string

## return

### success

```
{
    data:[
        {
            date: string (timestamp UTC)
            price: Number
            duration: int
            tutor: {
                firstName: string,
                lastName: string,
                photo: Blob/Image/Base64,
            },
            learner: {
                firstName: string,
                lastName: string,
                photo: Blob/Image/Base64,
            }

        },
        ...
    ]
}
```

### failure

# Get children sessions (futur/present/past)

GET sessions/children

## params

token: string

## return

### success

```
{
    sessions: [ sessions (see Get self sessions) ]
}
```

### failure


# Insert session
curl -X POST \
  http://localhost:8080/sessions \
  -H 'content-type: application/json' \
  -d ' {
      "date": 1551117271381,
      "price": 40,
      "duration": 2,
      "tutorId": "KS8XOlOrI",
      "learnerId": "2DY7UdEvdm",
      "status": 1
 }'


## returns :
{
    "data": "new_session_id"
}

# Update session
curl -X PUT \
  http://localhost:8080/sessions/KS8XOlOrI2 \
  -H 'content-type: application/json' \
  -d ' {
      "id": "KS8XOlOrI2",
      "date": 1551120864498,
      "price": 22,
      "duration": 3,
      "tutorId": "KS8XOlOrI",
      "learnerId": "2DY7UdEvdm",
      "status": 2
 }'
