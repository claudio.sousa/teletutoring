

# Connexion

POST authentication/login

## example:

curl -X POST \
  http://localhost:8080/authentication/login \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
      "username": "claudio",
      "password": "password"
    }'

## params

```
{
    username: string,
    password: string
}
```

## return

### success



### failure

Code: 100
Message: invalid username or password


# Déconnexion

GET authentication/logout

## params

token: string

