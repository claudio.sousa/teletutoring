# Best practices

https://restfulapi.net/resource-naming/

# Error

```
{
    error: {
        id: string,
        message: string
    }
}
```


# Profil learner

GET learner/{id}

## params

token: string
id: string

## return

### success

```
{
    first-name: string,
    last-name: string,
    email: string,
    last-connexion: string (timestamp UTC),
    photo: Blob/Image/Base64,
    languages: [ strings ]
}
```

### failure

Code: 201
Message: Learner not found



# Profil parent

GET parent/{id}

## params

token: string
id: string

## return

### success

```
{
    first-name: string,
    last-name: string,
    email: string,
    last-connexion: string (timestamp UTC),
    photo: Blob/Image/Base64,
    languages: [ strings ]
}
```

### failure

Code: 202
Message: Parent not found



