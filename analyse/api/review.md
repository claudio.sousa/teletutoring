
# Add a review

POST review/:userId

## Example:

curl -X POST \
  http://localhost:8080/reviews/KS8XOlOrI \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
      "rating": 3,
      "comment": "Super prof"
    }'

## params

userId: string

## Data

```
{
    rating: integer,
    comment: string
}
```