
Niveaux scolaires: curl 'http://localhost:8080/lookups/levels'
Matières: curl 'http://localhost:8080/lookups/subjects'
Langues: curl 'http://localhost:8080/lookups/languages'

## Response example (levels):
{
    "data": [
        "1P",
        "2P",
        "3P",
        "4P",
        "5P",
        "6P",
        "7P",
        "8P",
        "9P",
        "10P",
        "11P"
    ]
}