# Authentification

- En tant que visiteur sur la page d'accueil, je peux créer un compte en rentrant un nom d'utilisateur, un email et un mot de passe afin de pouvoir utiliser le site. Un compte est créé en résultat et l'utilisateur est loggé.


- En tant qu'utilisateur non connecté, possédant un compte, sur la page d'accueil, je peux me connecter sur le site à l'aide de mon email et de mon mot de passe. Si les informations présentées sont correctes l'utilisateur est loggé.

- En tant que parent connecté, je peux créer des comptes enfant, définir leur niveau scolaire et les langues qu'ils parlent

# Historique des leçons

- En tant que tuteur ou apprenant connecté, je peux afficher l'historique des sessions passées et consulter les documents échangés lors de celles-ci ainsi que les remarques et appréciations.

- En tant que parent connecté, je peux afficher l'historique des sessions passées de chacun de mes enfants et consulter les documents échangés lors de celles-ci ainsi les remarques et appréciations ainsi que leurs prix.

# Gestion des leçons 

- En tant que tuteur connecté, sur la page "Mes leçons", je peux consulter la liste des apprenants à qui j'ai donné des cours afin de trouver de différentes informations sur les séances et les apprenants.

- En tant que tuteur ou apprenant connecté, sur la page "Mes leçons", je peux initier une séance de télé-tutoring depuis les informations d'une séance qui s'apprête à avoir lieu.

- En tant que tuteur connecté, sur la page "Mes leçons", je peux consulter les demandes de séances initiées par des apprenants, les refuser ou les accepter et, si j'ai accepté, potentiellement proposer l'heure exacte du rendez-vous. L’apprenant reçoit le message de confirmation résultante.

- En tant que tuteur connecté, je peux mettre mon status comme "disponible tout de suite" afin de pouvoir recevoir des demandes de séances immédiates par des étudiants

- En tant que tuteur connecté, je peux laisser une remarque sur une séance à la destination de son parent

- En tant que apprenant connecté ayant juste fini une séance avec un tuteur, l'argent de mon parent est débité et je peux laisser une appréciation (note et un commentaire)  sur le tuteur. 

- En tant qu'apprenant connecté, je peux rejoindre une session initiée par un tuteur 

- En tant qu'apprenant connecté, je peux demander un rendez-vous immédiat avec un tuteur disponible maintenant


# Agenda et leçons futures

- En tant que tuteur connecté, sur la page "Agenda", je peux consulter mon agenda contenant les séances que j'ai réservé avec des apprenants, classées chronologiquement afin de préparer mes futures leçons.

- En tant que tuteur connecté, je peux définir mes disponibilités dans un calendrier, qui peuvent être récurrentes un certain nombre de fois ou jusqu'à une date

- En tant que tuteur ou étudiant connecté, je peux recevoir des demandes de replanification ou d'annulation de session par un tuteur ou étudiant et les accepter ou les refuser.

- En tant qu'apprenant connecté, je peux réserver une session avec un tuteur en proposant un jour et plage horaire 

# Profil

- En tant que tuteur connecté, sur la page "Mon profil", je peux définir les matières que j'enseigne selon des niveaux d'études et quel est le prix horaire de ces leçons. Ces informations ont une visibilité publique afin d'être recherchable par les apprenants.

 - En tant que apprenant connecté visualisant la page du profil d'un tuteur, je peux choisir de marquer le tuteur comme favori ou le mettre dans la "liste noire" afin de trouver ré-identifier plus tard les tuteur que j'aprécie et ceux que je n’apprécie pas. 
 
 - En tant que apprenant connecté visualisant la page d'un profile d'un tuteur, je peux envoyer un  message privé à ce tuteur.
 
- En tant que utilisateur connecté, sur la page de mon profil, je peux éditer les informations de mon profil afin des les mettre à jour. Les modifications sont préservées et visibles des autres utilisateurs.

- En tant qu'apprenant connecté, je peux changer mon niveau scolaire et mes langues

# Vérification de niveau

- En tant que tuteur connecté, sur la page "Mon profil", je peux uploader une copie d'un diplôme afin de faire vérifier que j'ai bien le niveau que je prétends avoir. Une fois une la vérification faite j'apparaît plus haut dans les résultats de recherche. 

 - En tant que administrateur connecté, je peux consulter les copies de diplômes soumises par les tuteurs afin de manuellement valider leur authenticité. 

# Finances

-  En tant que tuteur connecté, je peux consulter combien d'argent j'ai gagné à chaque leçon

- En tant que tuteur connecté, je peux consulter mes recettes hebdomadaires, mensuelles, annuelles

- En tant que tuteur connecté, je vois mon compte crédit avec l'argent d'une séance à la fin de celle-ci

- En tant que parent connecté, je peux recharger un compte avec des crédits afin que mes enfants puissent les utiliser pour suivre des leçons. Tous mes enfants partagent mon compte.

- En tant que parent connecté, je peux voir quel montant à été utilisé par enfant  à quel moment .

# Messagerie

- En tant qu'utilisateur connecté je peux recevoir des messages d'autres utilisateurs afin de planifier des leçons et d'échanger des informations au sujet des cours

# Recherche

- En tant qu'apprenant connecté, je peux rechercher un tuteur qui parle les même langues que moi, selon les matières qu'il dispense, et le niveau scolaire, son tarif horaire, ses disponibilités (jour de la semaine, matin/après-midi/soir). Je trouve mes tuteurs favoris, ou que j'ai bien noté par le passé, en haut des résultats. Les tuteurs que j'ai mal noté auparavant ne ressortent pas ou en bas dans mes recherches. Pour chaque tuteur, je peux trouve leur description et photo.








