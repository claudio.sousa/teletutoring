[Page des resources](https://gitlab.com/hepia-projects/teletutoring/wikis/Resources)

# Executer l'application

Assurez vous d'avoir les pré-requis:
 * [node.js](https://nodejs.org/en/)

Installer des dépendances:
```bash
cd src
npm install
```


Démarrer ba base de données
```bash
cd src/db
npm start
```


Démarrer le backend (après que la DB soit démarrée)
```bash
cd src/backend
npm start
```


Démarrer le frontend
```bash
cd src/frontend
npm start
```

Ouvrir l'application sur [http://localhost:4200](http://localhost:4200)

